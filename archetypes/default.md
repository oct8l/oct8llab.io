---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}

draft: true

toc: false

fontawesome: false

ogVideo:
ogAudio:
ogImage:

keywords: []

seoDescription:

summary:
url:

comments: false
---

