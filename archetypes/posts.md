---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}

draft: true

toc: false

fontawesome: false

ogVideo:
ogAudio:
ogImage:

tags: []
categories: []
keywords: []
seoDescription:

summary:

slug:

url:

### optional cover photo params ###
covertitle: false
cover:
coveropacity: 30%
covertext:
coverheight: 20vh
---

