---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}

draft: true

toc: false

fontawesome: false

ogVideo:
ogAudio:
ogImage:

seoDescription:
summary:

slug:
url:

---

