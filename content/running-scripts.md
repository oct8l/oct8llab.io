---
title: "Running Scripts"
draft: false
toc: false
tags: []
categories: []
keywords: []
seoDescription: 
summary: 
slug: 
url: 
comments: true
---

All scripts that I upload and reference on my Hugo site will have an associated checksum file along with it. I have a BASH alias that will calculate and output checksums into a text file that matches the script but with an appended `_checksum.txt`. For example, the [example script](/scripts/script-example.sh) of `script-example.sh`, there exists a [checksum file](/scripts/script-example.sh_checksum.txt) alongside it named `script-example.sh_checksum.txt`.

The four checksums calculated in this file are:

* MD5
* SHA1
* SHA256
* SHA512

Hopefully this should give enough for compatibility of automated systems (MD5 and SHA1) and for security (SHA256, SHA512). To ensure no scripts or checksums have been tampered with, please make sure to visit any pages with the http**s** protocol.

All scripts that I will upload will live in the `/scripts` directory of my site. You can visit this page [here](/scripts) and view all contents, including the checksums.