---
title: "Disclaimer"
date: 2018-07-02T07:15:21-05:00

draft: false

toc: false

fontawesome: false

seoDescription: 
summary: 

slug: 
url: /disclaimer/
---

You are 100% responsible for your own IT Infrastructure, applications, services and documentation. {{< friendly-base-url >}} owners, authors and contributors assume no liability or responsibility for your work. Please independently confirm anything you read on this blog before executing any changes or implementing new products or services in your own environment.

Furthermore, authors and contributors are free to express their opinions, positive or negative–they are, like all opinions, not reflective of any larger group, and do not represent the opinions of anyone else.