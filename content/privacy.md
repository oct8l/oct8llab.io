---
title: "Privacy"
date: 2018-07-02T07:15:27-05:00

draft: false

toc: false

fontawesome: false

seoDescription:
summary:

slug:
url: /privacy/
---

I am currently using Google Analytics on {{< friendly-base-url >}}. This is to track numbers of visitors and to see which content is the most popular. You can definitely use an extension such as uMatrix to block the JavaScript from loading and I wouldn't mind in the slightest! I have all options turned off as far as I can for tracking, but please visit the [Google Privacy Policy](https://policies.google.com/privacy) for more information.

This site is hosted with GitLab Pages, and is thus governed under the [GitLab.com Privacy Policy](https://about.gitlab.com/privacy/).

Comments are provided by a self-hosted Isso instance, which only logs partial IP information for purposes of spam reduction and moderation. Cookies are used to locally store previous names, email addresses and websites entered into the comment form. Those who don't engage in commenting or other discourse won't be subject to this.


The goal for this site is to be very light-weight, and private as I can make it by default. If you would like to contact me about any concerns or questions, please&nbsp;{{< contact-me >}}&nbsp;(my public GPG key is [here](https://oct8l.email/public.gpg.txt)).