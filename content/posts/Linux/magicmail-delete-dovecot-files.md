---
title: "Script to delete Dovecot files in MagicMail"
date: 2018-08-19T11:06:55-05:00

draft: false

toc: false

fontawesome: false

tags: ["dovecot","linux","script"]
categories: ["magicmail"]
keywords: ["mail","email","linuxmagic","magic"]
seoDescription:

summary: 

slug: magicmail-delete-dovecot-files-script

url: 
---

Every once in a while, Dovecot goofs itself up when customers are using multiple devices at the same time to check their mail, and we need to delete some files to get it to allow customers to view their emails again. This is partially a manual process until we're able to get it more automated, but the script below has made it a bit easier to deal with needing to delete them.

Basically, it can be run with a `check` param to check if the files that would potentially need deleted exist, and then if you run it without any params, it will go through the process of prompting for the location and then confirm you want to delete the `dovecot.index.lock`, `dovecot.index.cache` or `dovecot.index.log` files. It's hopefully pretty easy to read with all of my comments in it, so here's the script:

{{< highlight bash "linenos=table,linenostart=1" >}}
#!/bin/bash

# Written by oct8l (www.oct8l.com)
# https://www.apache.org/licenses/LICENSE-2.0

#
#

# This first if statment handles if you want to run the script to check if the
# files exist in the mailbox directory

# The syntax will be: ./unlock check /home/vpopmail/domains/[domain]/2/O/abc,123
#(if you were wanting to check the mailbox of my account)

if [ "$1" == "check" ]; then
	ls -l ${2}/Maildir | grep 'dovecot.index.log*\|dovecot.index.cache*\|dovecot.index.lock*' | awk '{print " "$6" "$7" "$8" "$9" "}' && exit;
fi


# This is the main part of the script to prompt you for the path of the user's
# mailbox that you can find in the Magic Mail admin page under Mailbox Info

echo "Enter the mailbox's path"
read mailboxpath
cd $mailboxpath/Maildir

# This will tell you what will be deleted by the script if you give confirmation
# in the next step

echo 
echo "-------------------------------------------------"
echo "Files up for deletion:"
echo 
ls -l | grep 'dovecot.index.log*\|dovecot.index.cache*\|dovecot.index.lock*' | awk '{print " "$6" "$7" "$8" "$9" "}'
echo 
echo "-------------------------------------------------"
echo 

# This is where you can answer y, yes, Yes, yEs, yeS, YeS, or YES if you'd like
# to go ahead and delete the files, or the script will exit if any other input
# is received
read -r -p "Would you like to delete dovecot.index.log, dovecot.index.cache and dovecot.index.lock? [y/N] " response
if [[ "$response" =~ ^([yY][eE][sS]|[yY])+$ ]]
then
    rm dovecot.index.log* ; rm dovecot.index.cache ; rm dovecot.index.lock ; ls -l | grep 'dovecot.index.log*\|dovecot.index.cache*\|dovecot.index.lock*' | awk '{print " "$6" "$7" "$8" "$9" "}';
else
    echo "Exiting script" && exit;
fi

exit
{{< / highlight >}}

Try it out if you by chance run MagicMail and run into these same issues! I'll update this post when I'm able to get this fully automated.