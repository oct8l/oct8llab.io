---
title: "Dexit (Docker EXIT) alias for Docker Toolbox!"
date: 2019-01-07T14:47:05-06:00

draft: false

toc: false

fontawesome: false

ogVideo:
ogAudio:
ogImage:

tags: ["docker","linux","bash"]
categories: ["Docker Toolbox"]
keywords: ["windows","shutdown","vm"]
seoDescription:

summary: 

slug: 

url: 
---

If you use Docker Toolbox for Windows, there's an easier way to get everything shut down than going into the VirtualBox GUI and shutting down the Docker VM (if that's how you've been doing it). There is a command called `docker-machine` that's available when in the Docker BASH prompt that can control a lot of the host VM's functions. In fact, Docker's `start.sh` that runs when opening the BASH prompt uses it to configure everything inside of VirtualBox on startup.

I've gone into my `~/.profile` file and added an alias for `dexit` ("Docker EXIT") that will turn the VM off and close the BASH window. Try adding the following to your Docker Toolbox's profile!

``` sh
alias dexit="docker-machine stop && exit"
```

It works real slick to shutdown my Docker Toolbox BASH when I'm all done containerizing things unnecessarily.