---
title: "Raspberry Pi Web Kiosk"
date: 2019-03-27T14:17:44-05:00

draft: true

toc: false

fontawesome: false

ogVideo:
ogAudio:
ogImage:

tags: ["bash","curl","script","linux"]
categories: ["raspberry pi"]
keywords: ["kiosk","customer portal","payment","pci"]
seoDescription:

summary:

slug:

url:

### optional cover photo params ###
covertitle: false
cover:
coveropacity: 30%
covertext:
coverheight: 20vh
---

