---
title: "Disqus Commenting"
date: 2018-03-16T18:28:27-05:00
draft: false
toc: false
keywords: []
seoDescription:
summary:
slug:
tags: ["disqus", "commenting"]
categories: ["Hugo site"]
url:
---

{{% update "15 Apr 2019" br %}}

I have replaced Commento's functionality with <a href="https://posativ.org/isso/" target="_blank">Isso</a>.

{{% /update %}}

{{% update "11 Jan 2019" br %}}

I have re-deployed Commento, and it is working again with my GitLab Pages site.

{{% /update %}}

{{% update "11 Nov 2018" br %}}

Commento has stopped working on my VPS and I have disabled it while I try to troubleshoot and rebuild it.

{{% /update %}}

{{% update "15 May 2018" br %}}

Disqus has been replaced by <a href="/posts/2018/129/setting-up-commento-with-hugo/" target="_blank">Commento</a>.

{{% /update %}}

<hr>

Here on my Hugo site, I decided to go with Disqus for a commenting system.

This wasn't my first or favorite choice, as it means more JavaScript to load on the posts, and it makes it confusing and a [bit difficult to comment as a guest](https://disqus.com/home/discussion/channel-discussdisqus/community_growth_guest_commenting_as_default/). Maybe my least favorite part, though, is storing the data through another service. One of the reasons I love Hugo is the ability to get a single .zip file of the source and move all content to another hosted service very quickly. With comments being hosted on Disqus, there is a separate migration process to make sure all comments follow the content.

Previously, I had looked at a few different options that the [official Hugo docs](https://gohugo.io/content-management/comments/) suggested, including [Static Man](https://staticman.net/) and [IntenseDebate](http://intensedebate.com/). The reason I ended up ruling out IntenseDebate was that it is essentially the same offering as Disqus, but with less documentation. I really wished Static Man would work, but it required the site to be on GitHub pages, and I ended up choosing GitLab pages to deploy my Hugo site mostly for ease of deployment.

So why even implement commenting at all if Disqus isn't a great solution?

There are a couple of reasons. First, I want to be able to be called out if I get something wrong that I'm trying to post about. Second reason is someone is always going to know more than me on a given topic, and if they run across one of my posts it would be awesome for that person to be able to share their knowledge.

If you are using uMatrix or similar (which I think everyone should), this is what your whitelist for this site should look like this:

<center>![uMatrix Whitelist](/img/posts/2018-75-disqus-commenting.png)</center>

[^commento-post]: <a href="/posts/2018/129/setting-up-commento-with-hugo/" target="_blank">{{< base-url >}}posts/2018/129/setting-up-commento-with-hugo/</a>