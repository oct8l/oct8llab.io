---
title: "Docker Image for building Hugo site with GitLab CI"
date: 2018-03-21T20:38:22-05:00
draft: false
toc: false
keywords: []
seoDescription:
summary:
slug:
tags: ["docker", "gitlab-ci", "hugo"]
categories: ["Hugo site"]
url:
---

When I was getting my Hugo site set up, I was looking for a way to implement search. Luckily, it seems that I ran across [a solution by eddiewebb](https://gist.github.com/eddiewebb/735feb48f50f0ddd65ae5606a1cb41ae) and I was able to get it set up.

Originally, I had issues where it would work locally, but when I would push it up to GitLab pages, CI would take over and publish a version of the site, but the search function didn't work when it was live. After downloading a couple different versions of Hugo locally and trying out `hugo server`, it appeared that there were functions introduced between `0.32.3` and `0.37.1` that the search function depended on. I had been using the default template from GitLab for the [pages CI script](https://gitlab.com/pages/hugo/blob/e99a9eaf6819566deb1755f8f87f580fa95980ab/.gitlab-ci.yml) that was only at `0.32.3` at the time of publishing my site with GitLab Pages.

I decided to take the opportunity to create and host [my own image](https://gitlab.com/oct8l/oct8l.gitlab.io/container_registry) in the repo for the site with newer versions. As of writing, it is running `0.37.1` and I will aim to update it as newer versions come out and newer features are added to Hugo. After making this image, [the search](/search/) seems to be working perfectly.

To edit the default template from GitLab Pages to use a newer version of Hugo, I just had to change the `image` declaration to this:

{{< highlight yaml "linenos=table,linenostart=5" >}}

image: registry.gitlab.com/oct8l/oct8l.gitlab.io/hugo:latest

{{< / highlight >}}

Feel free to use these containers as I continue to build and push them to this registry!

