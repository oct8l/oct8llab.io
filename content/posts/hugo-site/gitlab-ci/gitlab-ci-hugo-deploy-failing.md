---
title: "Gitlab CI Hugo Deploy Failing"
date: 2018-03-25T14:12:10-05:00
draft: false
toc: false
keywords: []
seoDescription: I had been having issues where GitLab-CI pipelines for this Hugo site would fail on the deploy function. This little CI trick fixed that.
summary:
slug:
tags: ["gitlab-ci", "hugo"]
categories: ["Hugo site"]
url:
---

For a while, I had been having issues where GitLab-CI pipelines for this Hugo site would fail on the deploy function. This would let the site build correctly, but the files wouldn't be pushed to the location where they were served to visitors. There are [a few open issues](https://gitlab.com/gitlab-org/gitlab-ce/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=CI%2FCD&search=pages+deploy+failed)  where this same behavior is exhibited, but I did get a tip from [@fyr77](https://gitlab.com/gitlab-org/gitlab-ce/issues/34899#note_64358104) on how they were able to resolve the issue for their own project.

I adapted this solution in my [.gitlab-ci.yml file](https://gitlab.com/oct8l/oct8l.gitlab.io/blob/fdd3c07dab8/.gitlab-ci.yml#L14-17):

{{< highlight yaml "linenos=table,linenostart=14" >}}

  script:
  - mkdir .public
  - hugo -d .public
  - mv .public public

{{< / highlight >}}

What this does is:

1. Make a directory called `.public`
2. Build the Hugo site into the `.public` directory
3. Rename the `.public` directory to `public` as it normally is built into

It appears the issue was somewhere with building all files into the `public` directory correctly, because after trying this out, builds all seem to work correctly now with no deploy fails.
