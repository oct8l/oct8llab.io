---
title: "Setting up Commento with Hugo"
date: 2018-05-09T18:38:46-05:00
draft: false
toc: true
tags: ["commenting", "commento"]
categories: ["hugo-site"]
keywords: ["chicagovps"]
seoDescription: After some though, Disqus really isn't the ideal commenting solution. Enter Commento.
summary:
slug:
url:
fontawesome: true
---

{{% update "15 Apr 2019" br %}}

While Commento served its purpose fantastically, I have moved on to <a href="https://posativ.org/isso/" target="_blank">Isso</a>.

{{% /update %}}

{{% update "11 Nov 2018" br %}}

I have Commento working again on the site.

{{% /update %}}

{{% update "11 Jan 2019" br %}}

Commento has stopped working on my VPS and I have disabled it while I try to troubleshoot and rebuild it.

{{% /update %}}

<hr>

As I thought about it more, I realized that I didn't like the idea of using Disqus for a commenting here on my Hugo site. This was for a few different reasons, but mainly because reduced privacy and tracking users across the internet [^patrickleenyc] [^chrislema] [^businessinsider] are just part of the Disqus package.

[^patrickleenyc]: https://medium.com/patrickleenyc/beware-of-disqus-17fb58cfab10
[^chrislema]: http://chrislema.com/killed-disqus-commenting/
[^businessinsider]: https://www.businessinsider.com.au/disqus-launches-advertising-2014-11

This led me back out on a search for an alternative for commenting that could still be useful without trading off privacy or lack of tracking.

I just so happened to run across [a Reddit post](https://www.reddit.com/r/linux/comments/8hrz9s/counting_usernames_from_malicious_login_attempts/) of a static-looking site that had some nice looking commenting at the bottom. And since I have the trusty uMatrix extension in all of my browsers, I could see that this site was requesting to reach out to a subdomain of the main site called `commento`. After a quick search, I had found what seemed like a perfect commenting solution.

# The new solution

[Commento](https://github.com/adtac/commento) is a commenting system that has the ability to be self-hosted, which I always love. Its main benefits are having a tiny payload when loading comments on a page and having no sort of tracking whatsoever. It is maintained by [adtac](https://github.com/adtac) on GitHub, and seems to still be pretty active. He has talked about a hosted solution in the future, but as long as it retains the ability to self-host, I will stick with that route.

# Getting a VPS

Realizing I'd have to host it on my own to get this up and going, I racked my brain for the cheapest but most reliable way I could do this. Luckily, I had run across a provider named [ChicagoVPS](https://billing.chicagovps.net/aff.php?aff=2325) [^chicago-vps-aff] that had dollar-a-month (!) VPSes with low resources that would perfectly fit the bill. I went with a minimal CentOS 7 install, which is easy on the 128MB of RAM. If you're planning on using Commento, I think this is probably the best way to get it done.

[^chicago-vps-aff]: This is an **optional** affiliate link

# Getting and running Commento

From your terminal, enter the following commands:

{{% alert "purple" %}}

<i class="fas fa-exclamation-circle"></i>&nbsp; I have updated the download link directly below with a fork that I have made (of a fork of Commento) when the binaries were still available.

{{% /alert %}}

`wget https://github.com/oct8l/commento/releases/download/v0.3.1.0.1/commento.tar`

`tar xvf commento.tar`

`rm -rf commento.tar`

`./commento &`

This will download, unzip, remove the tar.gz file, and then run Commento in the background on the default port 8080. You can also specify the port with the `COMMENTO_PORT` param, for example; `COMMENTO_PORT=80 ./commento &`. Using this command allowed me to test accessing Commento over plain HTTP on port 80 for a proof of concept.

Unfortunately, modern browsers are just too dang secure and were having issues loading the insecure JavaScript from Commento over port 80/http. The section at the bottom of posts wouldn't load, and Chrome would give me a little shield up in the address bar (Firefox also showed this same "issue"):

![Unsafe content alert in Chrome](/img/posts/2018-129-setting-up-commento-with-hugo-ssl-error.png)

But at least this meant it was working!

At this point you can go ahead and kill the Commento process. Run `pgrep commento` to find the PID, and then run `kill -9 PID` and replace PID with the number printed by the last command (e.g. `kill -9 4422`).

# Setting up a subdomain

I have my domain of oct8l.com registered with Namecheap, which I highly recommend. All support requests have been handled very quickly, and the prices really can't be beat in most cases.

Most registrars have their own instructions, including [Namecheap](https://www.namecheap.com/support/knowledgebase/article.aspx/9776/2237/how-do-i-create-a-subdomain-for-my-domain). Just Google away, or search your registrar's knowledge base.

If you wait a few minutes (Namecheap says 30) to a couple hours, you should be able to `dig` or `ping` your new hostname and get your VPS's IP as a response. I would wait for this to propagate before going on to the next step.

# Configuring HTTPS

My first thought to get the issue with browsers blocking insecure content cleared up (and more secure), was to install Nginx to accept the secure traffic on port 443/https, and then proxy to Commento inside of the VPS. I've used this solution before (thanks to [a cool web series](https://www.youtube.com/watch?v=1OU5ngq-WyM&list=PLQlWzK5tU-gDyxC1JTpyC2avvJlt3hrIh) from Juriy Bura), so I figured it would be relatively painless to get set up. Since I chose CentOS 7 minimal for the OS on my ChicagoVPS, I followed the wonderful [tutorial](https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-centos-7) from DigitalOcean for getting it installed.

An awesome perk of using Nginx is that it gives us a method for automating our SSL cert setup and renewal process with the help of EFF's [Certbot](https://certbot.eff.org/) This will keep the total yearly cost of this libre commenting at $12, which I don't mind for a fun little project.

So now that we have Nginx installed, we'll run Certbot and have it set up a Let's Encrypt certificate for us.

Enter `certbot --nginx` into your VPS's terminal and enter your domain name you have registered in the previous step when prompted, `commento-gitlab-pages.oct8l.com` for example.

Tell Certbot to go out and register you a new certificate, and then tell it to not create an HTTPS redirect automatically for you. I had issues when I accidentally selected the option to create the redirect the first time around.

## Nginx config

Back on your VPS, use `vi` (or run `yum -y install nano` and use `nano` if you'd like to be able to exit your editor) to look at the default config file for Nginx, which should be at `/etc/nginx/nginx.conf`. You can also add a secondary config to `/etc/nginx/conf.d/XXXX.conf` but I found it just as easy to edit the main config.

Yours should look similar to what I have below, with the exception of the domain name used. I have highlighted the lines that I changed, and I suggest you change your config file to match mine.

{{% alert "blue" %}}

<i class="fa fa-info-circle"></i> **Note:** I had removed most of the content in the middle of the config pasted below, as Certbot made entries at the bottom (see lines containing `# managed by Certbot`) that duplicated existing entries

{{% /alert %}}<br>

{{< highlight sh "linenos=table,hl_lines=11 36 39 40 41 48 62 63,linenostart=1" >}}

# For more information on configuration, see:
#   * Official English Documentation: http://nginx.org/en/docs/
#   * Official Russian Documentation: http://nginx.org/ru/docs/

user nginx;
worker_processes auto;
error_log /var/log/nginx/error.log;
pid /run/nginx.pid;

# Load dynamic modules. See /usr/share/nginx/README.dynamic.
# include /usr/share/nginx/modules/*.conf;

events {
    worker_connections 1024;
}

http {
    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;

    sendfile            on;
    tcp_nopush          on;
    tcp_nodelay         on;
    keepalive_timeout   65;
    types_hash_max_size 2048;

    include             /etc/nginx/mime.types;
    default_type        application/octet-stream;

    # Load modular configuration files from the /etc/nginx/conf.d directory.
    # See http://nginx.org/en/docs/ngx_core_module.html#include
    # for more information.
    # include /etc/nginx/conf.d/*.conf;

    server {
        listen       80 ;
        listen       [::]:80 ;
        server_name  YOUR-DOMAIN-HERE; # managed by Certbot
        root         /usr/share/nginx/html;

        # Load configuration files for the default server block.
        include /etc/nginx/default.d/*.conf;

        location / {
	        proxy_pass "http://127.0.0.1:8080/";
        }

        error_page 404 /404.html;
            location = /40x.html {
        }

        error_page 500 502 503 504 /50x.html;
            location = /50x.html {
        }


    listen [::]:443 ssl ipv6only=on; # managed by Certbot
    listen 443 ssl; # managed by Certbot
    ssl_certificate /etc/letsencrypt/live/YOUR-DOMAIN-HERE/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/YOUR-DOMAIN-HERE/privkey.pem; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot

}}

{{< / highlight >}}

This config will listen on ports 80 and 443 for traffic, and then send that traffic to the internal port 8080 that Commento is listening on for commenting traffic.

After making the changes, exit your editor and run `nginx -s reload` to reload Nginx with the new config file. You should then be able to go to the FQDN of your new subdomain through port 443/HTTPS and see the [JavaScript and CSS files](https://commento-gitlab-pages.oct8l.com/assets/). We're almost done!

## Automating certificate renewal

If you went with the CentOS 7 Minimal installation like I had, you'll need to do a little bit extra work for setting up the automatic renewal of the Let's Encrypt cert.

You'll need to install the `cronie` package, so run the following commands:

`yum install cronie`

`systemctl start crond`

`systemctl enable crond`

This will install the package, start the cron daemon, and enable it on boot so it'll automatically start when CentOS does.

You should now be able to run `crontab -e` to edit your cron file. Add something similar to this line:

`42 6 * * * certbot renew`

This uses `vi` by default, so enter the following sequence to get out: `ESCAPE` `:wq` `ENTER`

This specific line would have your machine go out at 6:42 local (to the VPS) time and check if the certificate is close to expiring. If so, Certbot will take over and renew your certificates for you. Otherwise, it'll just wait until the next day.

You can change this time to something random, and [check with crontab.guru](https://crontab.guru/#42_6_*_*_*) to make sure it's valid.

# Adding the comments to your Hugo site

In Hugo, add a partial under `layouts/partials/` named `commento.html` with the following HTML:

{{< highlight sh "linenos=table,hl_lines=3,linenostart=1" >}}

<div id="commento"></div>
<script defer
  src="https://YOUR-DOMAIN-HERE/assets/js/commento.js"
  data-div="#commento">
</script>

{{< / highlight >}}

{{% alert "blue" %}}

<i class="fa fa-info-circle"></i> **Note:** Make sure to replace your subdomain on the highlighted line

{{% /alert %}}<br>

Now add `{{ partial "commento.html" . }}` in your `single.html` default layout ([like I have implemented](https://gitlab.com/oct8l/oct8l.gitlab.io/blob/676a8a66ddb62d5328f61e8033e8c1ad34622acc/themes/crab/layouts/_default/single.html#L46)), or wherever you would like comments on your site to be displayed.

Now go ahead and push that code to CI or regenerate your site, and you should have secure and private commenting in Hugo!