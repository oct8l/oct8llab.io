---
title: "Making a knockoff WiFi Pineapple from a GL-iNet AR150"
date: 2019-02-23T20:39:44-06:00

draft: true
toc: true
fontawesome: true

tags: ["gl-inet","ar150m"]
categories: ["security"]
keywords: ["pineapple","wifi"]
seoDescription: For the wannabe hacker with a champagne taste on a lemonade budget. I was looking for a way to have handy tools at an even more handy price.
summary:
slug: making-a-knockoff-wifi-pineapple-from-a-gl-inet-ar150m
url:

covertitle: true
cover: /img/posts/covers/pineapple-supply-co-273920-unsplash.jpg
coveropacity: 45%
covertext:
coverheight: 22vh
---

While I have known about the [GL-iNet AR150](https://www.gl-inet.com/products/gl-ar150/)'s ability to mimic a [WiFi Pineapple](https://www.wifipineapple.com/) Nano, I hadn't had an extra laying around to play with, until I saw they they were on sale on Amazon (15%, but still a sale) and decided I'd like a weekend project.

It arrived, and even before I turned booted it into its native (and really awesome) stock GL-iNet stock firmware, I performed the steps to get it to boot into Uboot mode and uploaded the firmware that was compiled by [al3xg on Security Addicted](https://www.securityaddicted.com/2016/11/17/weaponizing-gl-inet-gl-ar150/). This was great and all, but I noticed on the page that they had deprecated the project and I was wanting to see how far I can go to get this close to the current version of the WiFi Pineapple firmware as possible before getting caught by the reported hardware checks al3xg mentioned on the Security Addicted post.

There are a surprising number of great resources out there on this process, so I'd like to say thank you to [Patrick Sapinski](http://sapinski.com/2016/02/13/wifi-pineapple-firmware-for-gl-inet-gl-ar150/), [KhasMek on GitHub](https://khasmek.github.io/howto/2016/09/27/ar150-pineapple-install), [tomac on Medium](https://medium.com/@tomac/install-openwrt-or-pine-apple-on-low-cost-wifi-router-67cbd26a1a15), and [Carson Seese](https://blog.carsonseese.com/gl-ar150-pineapple/) for everything they've researched, found out, and published for anyone to look up and read.

While all of the guides I had found were referencing building the new firmware with the old [Domino Team OpenWrt-CC](https://github.com/domino-team/openwrt-cc) repo (Domino Team was the precursor to GL-iNet I believe, but don't quite me on it), I happened across the newer [imagebuilder-cc-ar71xx](https://github.com/gl-inet/imagebuilder-cc-ar71xx) repo and decided to take a crack at using the newer version. To start with, I tried out the [version 2.0.2 WiFi Pineapple firmware](https://www.wifipineapple.com/downloads#nano-10), as that was confirmed working by al3xg on Security Addicted.

# Building the firmware

## Grab your files
Your first steps are to get these files on your local machine.

<sub class="inline">Follow along with the [Asciicasts](https://asciinema.org/) below (you can pause to copy and paste text straight out of the terminals if you'd like)</sub>

{{< asciicast LxEH9omaFRx8bTniCCRn7W7yW >}}

## Extract with `binwalk`
Next, you'll want to use [binwalk](https://github.com/ReFirmLabs/binwalk) with the `-e` flag to **e**xtract the contents of the WiFi Pineapple firmware.

{{< asciicast reQNeXrlIt4H6BOTvGBqK5CSS >}}

## Copy your files
After this, you'll want to copy the contents of the `squashfs-root` folder into a folder called `files` inside of your `imagebuilder-cc-ar71xx` git directory that you cloned earlier (you'll need to create this folder).

{{< asciicast JK930D5ycYLsogVQ2cC4rSv8P >}}

## Build the firmware
After that, you'll just want to build the firmware with `imagebuilder-cc-ar71xx`. I have chosen to run the command `make image PROFILE=GL-AR150 PACKAGES="kmod-rtl8xxxu kmod-rtlwifi-usb" FILES=files/`, as I'm building for the GL-iNet AR150 and I want to include the modules for Realtek USB adapters for the [Netgear AC 6100 adapter](https://www.netgear.com/home/products/networking/wifi-adapters/A6100.aspx) I had on hand.

You can visit the fantastically well-documented [OpenWrt wiki page](https://openwrt.org/packages/index/kernel-modules---wireless-drivers) to find more kernel modules you might want to install in the `PACKAGES=""` section of the command above.

{{% alert "blue" %}}

<i class="fa fa-info-circle"></i>&nbsp;I recorded this terminal session with Asciinema, and it has a feature to reduce "dead air" to one second, so this seems to go by in a flash. In reality, this build took about 15-20 minutes.

{{% /alert %}}

{{< asciicast luoVkUjeL8QvzdC5lnGmfFCTy >}}

# Loading the firmware

## Booting into Uboot
You'll now want to follow the instructions from GL-iNet to [boot into the Uboot interface](https://docs.gl-inet.com/en/3/troubleshooting/debrick/). In summary, you'll want to:

1. Unplug any power cable that's powering the device
2. Hold down the **Reset** button on the side
3. Plug the Micro USB power in while holding the button
  * you will hold until the red LED flashes **five** times
4. Release the **Reset** button

Your router should now be booted to the Uboot interface.

## Uploading the firmware

You will want to set your local computer's IP address to `192.168.1.2` (subnet of 255.255.255.0) in order to communicate with the router when it's booted into Uboot.

If you browse to http://192.168.1.1, it should load a page similar to this:

![Uboot interface](/img/posts/2019-54-making-a-knockoff-wifi-pineapple-from-a-gl-inet-ar150m/uboot.png)

Click the **Browse...** button, and you will be able to navigate your file system and select the firmware you have just generated. Click **Update firmware** to start the process.

The page will load for a second or two, and then you will get to this screen:

![Uboot spinning wheel](/img/posts/2019-54-making-a-knockoff-wifi-pineapple-from-a-gl-inet-ar150m/spinning.gif)

You can now close your browser window and change your ethernet interface back to DHCP if you'd like.

# Accessing PineAP

## Connecting to your new Pineapple

### Connecting over wireless

On your device, search for a new open wireless network named `Pineapple_wxyz` where `wxyz` will be the last four of your router's MAC address. Once connected, open a browser and head to http://172.16.42.1:1471 to get to the web GUI of the pineapple.

### Connecting over ethernet

It seems that in the building process, the WAN and LAN ports get swapped (or perhaps the LAN port is disabled completely -- I haven't confirmed it either way yet). If you'd like to connect via ethernet, plug into the WAN port, and head to http://172.16.42.1:1471 to get to the web GUI of the pineapple.

## Initial setup of PineAP

On the initial welcome screen, go ahead and click the **Get Started** button to start the setup.

![Get started](/img/posts/2019-54-making-a-knockoff-wifi-pineapple-from-a-gl-inet-ar150m/get-started.png)

When you get to the _Secure Setup_ screen, you will be prompted to either press the button once to disable wifi, or hold the reset button to continue with wifi.

If you chose to connect to the pineapple via wifi earlier, you will want to hold the same **Reset** button on the side of the router that you held to get into Uboot earlier for _at least_ two seconds. The screen will automatically advance past this step after holding the button. Otherwise, you can just press the reset button one quick time. _It doesn't hurt to leave the wireless enabled, so that is what I will recommend._

![Wifi choice](/img/posts/2019-54-making-a-knockoff-wifi-pineapple-from-a-gl-inet-ar150m/wifi-choice.png)

Next, you will want to enter the admin password for the pineapple, an unsuspecting SSID, and a password for the management SSID.

![Management password and wifi](/img/posts/2019-54-making-a-knockoff-wifi-pineapple-from-a-gl-inet-ar150m/management-wifi-and-password.png)

Agree to any other information, and **Complete Setup**.

After changing the management SSID, you will get an error in your browser, and you will need to reconnect to the new SSID name you just created.

<center>![HTTP error](/img/posts/2019-54-making-a-knockoff-wifi-pineapple-from-a-gl-inet-ar150m/http-error.png)</center>

Reconnect to the new SSID, browse back to http://172.16.42.1:1471, enter your new admin password, and now you can start using your pineapple!

# Closing

Usage of the pineapple is beyond the scope of this post, but there is a lot of information out there for how to do all sorts of things with your new toy.

KhasMek, the author of one of the other articles I referenced at the beginning, has a [repo on GitHub](https://github.com/KhasMek/gl-ar150-pineapple-firmware-builder) that is made to be an all-in-one script to support doing all of the firmware building steps above in a single shell script. I had some issues with it, but I think it's an awesome idea and a lot of great work. Maybe some day I will fork and update it with all of the steps that I have above.

# Troubleshooting

Other than that, all I can suggest is that Google is your friend.

Happy WiFi Pineappleing!