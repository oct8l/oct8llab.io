---
title: "Relaxed Theme for Cmder"
date: 2019-01-10T19:40:02-06:00

draft: false

toc: false

fontawesome: true

ogVideo:
ogAudio:
ogImage:

tags: ["relaxed theme","cmder"]
categories: ["software"]
keywords: ["theme","colors"]
seoDescription:

summary:

slug:

url:

### optional cover photo params ###
covertitle: true
cover: /img/posts/covers/2019-8-royal-ts-relaxed-theme.png
coveropacity: 40%
covertext:
coverheight: 21vh
---

Just as I translated the [Relaxed Theme](https://github.com/Relaxed-Theme) to [Royal TS](/posts/2019/8/relaxed-theme-for-royal-ts/), I've also made a color scheme in [Cmder](http://cmder.net/). I absolutely love Cmder, it's become my replacement on my work laptop for the default CMD, PowerShell, Git for Windows, WSL Docker Toolbox, and basically any CLI available on Windows.

Anyway, head on over to your Cmder XML file (check the top of your Cmder settings screen), `CTRL + F` search for "Palette", and append the following text to the end of the XML file:

{{% alert "yellow" %}}

<i class="fas fa-bell"></i> **Notice**: Change the string in the highlighted line 1 from `XYZ` to the next number in line, such as 2 or 3 if you only have 1 or 2 other schemes.

{{% /alert %}}<br>

{{< highlight sh "linenos=table,hl_lines=1,linenostart=1" >}}

<key name="PaletteXYZ" modified="2019-01-10 20:13:33" build="180528">
    <value name="Name" type="string" data="Relaxed"/>
    <value name="TextColorIdx" type="hex" data="10"/>
    <value name="BackColorIdx" type="hex" data="10"/>
    <value name="PopTextColorIdx" type="hex" data="10"/>
    <value name="PopBackColorIdx" type="hex" data="10"/>
    <value name="ColorTable00" type="dword" data="00151515"/>
    <value name="ColorTable01" type="dword" data="00ffdfc9"/>
    <value name="ColorTable02" type="dword" data="00639d90"/>
    <value name="ColorTable03" type="dword" data="0099876a"/>
    <value name="ColorTable04" type="dword" data="005356bc"/>
    <value name="ColorTable05" type="dword" data="009866b0"/>
    <value name="ColorTable06" type="dword" data="007adaeb"/>
    <value name="ColorTable07" type="dword" data="00d9d9d9"/>
    <value name="ColorTable08" type="dword" data="00d9d9d9"/>
    <value name="ColorTable09" type="dword" data="00dac19a"/>
    <value name="ColorTable10" type="dword" data="008bdac9"/>
    <value name="ColorTable11" type="dword" data="00ffeee3"/>
    <value name="ColorTable12" type="dword" data="007b7edb"/>
    <value name="ColorTable13" type="dword" data="00d3a5e9"/>
    <value name="ColorTable14" type="dword" data="00a5e7f3"/>
    <value name="ColorTable15" type="dword" data="00f7f7f7"/>
</key>

{{< / highlight >}}
Here's where the code snippet will fit into the XML file:

![Colors in the Cmder config](/img/posts/2019-10-cmder-relaxed-theme.png)

Happy Relaxing!