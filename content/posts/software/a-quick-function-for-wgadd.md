---
title: "Quick BASH function for adding WireGuard peers"
date: 2019-02-15T21:50:34-06:00

draft: false

toc: false

fontawesome: false

ogVideo:
ogAudio:
ogImage:

tags: ["wireguard","bashrc","function"]
categories: ["wireguard"]
keywords: ["peer","script"]
seoDescription:

summary:

slug:

url:
---

If you're like me and diving into WireGuard as it picks up more attention, you might be adding lots of WireGuard peers to a server as you're figuring it out and playing around with it. On my server box, I added a function to my `.bashrc` to make it a bit easier to add peers.

If you'd like, copy the following (three, and simple at that) lines into your `.bashrc`:

{{< highlight sh "linenos=table,hl_lines=0,linenostart=1" >}}

wgadd () {
        sudo wg set wg0 peer $1 allowed-ips 10.20.30.$2/32
}

{{< / highlight >}}

Then either log out or run `source ~/.bashrc` (or `source` from wherever _your_ file is), and then run it with this example command:

``` sh
wgadd aSM/g5dtoYAC9OgeWUWnRYxt0oeyRQu3DejJKEIdNSk= 89
```

This would run the command `sudo wg set` command on the WireGuard interface `wg0` and add a peer with the public key of `aSM/g5dtoYAC9OgeWUWnRYxt0oeyRQu3DejJKEIdNSk=` allowed with the IP of `10.20.30.89`.

You can adjust any of the bits of the function (mostly the interface name of `wg0` and the IP range of `10.20.30.x`) if needed on your system.