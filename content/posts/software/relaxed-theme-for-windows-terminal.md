---
title: "Relaxed Theme for Windows Terminal"
date: 2021-05-25T22:04:26-05:00

draft: false

toc: false

fontawesome: false

ogVideo:
ogAudio:
ogImage:

tags: ["relaxed theme","windows terminal"]
categories: ["software"]
keywords: ["theme","colors","windows terminal"]
seoDescription:

summary:

slug:

url:

### optional cover photo params ###
covertitle: true
cover: /img/posts/covers/2019-8-royal-ts-relaxed-theme.png
coveropacity: 40%
covertext:
coverheight: 21vh
---

Much like how I've set up [Relaxed Theme](https://github.com/Relaxed-Theme) for [Royal TS](/posts/2019/10/relaxed-theme-for-cmder/) and for [Cmder](/posts/2019/10/relaxed-theme-for-cmder/), I've started using Windows Terminal in Windows instead of Cmder, so I wanted to figure out how to make my own theme for WSL so it looks how I like it.

After downloading [Windows Terminal](https://github.com/microsoft/terminal), you can edit your settings JSON file by going to settings (hotkey of `CTRL` + `,`) and clicking the "Open JSON file" in the bottom left.

After you have this open, you can drop in the following section to create a theme named "Relaxed."

{{< highlight json "linenos=table" >}}

        {
            "background": "#353A44",
            "black": "#353A44",
            "blue": "#6A8799",
            "brightBlack": "#7F7F7F",
            "brightBlue": "#9AC1DA",
            "brightCyan": "#E3EEFF",
            "brightGreen": "#C9DA8B",
            "brightPurple": "#E9A5D3",
            "brightRed": "#DB7E7B",
            "brightWhite": "#DB7E7B",
            "brightYellow": "#F3E7A5",
            "cursorColor": "#D9D9D9",
            "cyan": "#C9DFFF",
            "foreground": "#D9D9D9",
            "green": "#909D63",
            "name": "Relaxed",
            "purple": "#B06698",
            "red": "#BC5653",
            "selectionBackground": "#FFFFFF",
            "white": "#F7F7F7",
            "yellow": "#EBDA7A"
        },

{{< / highlight >}}<span class="brsmall"></span>

<sup class="code">Mine is in the middle of my config file, but if you put it at the end of the file, be sure to remove the trailing comma after the curly brace or your Windows Terminal will throw a fit</sup>

You should have something that looks like this after pasting:

<center>![JSON file after adding theme](/img/posts/2021-145-relaxed-theme-for-windows-terminal/json-file.png)<br><br></center>

After dropping this in your config, go back to the settings window in Windows Terminal, and select the terminal you'd like to apply the theme to from the "Profiles" section in the left navigation, and head to the *Appearance* tab. From there, you should now have an option in the dropdown section for Relaxed.


<center>![Windows Terminal theme options](/img/posts/2021-145-relaxed-theme-for-windows-terminal/windows-terminal-settings.png)<br><br></center>

That's it! Now you can enjoy the Relaxed theme in any of the supported Windows Terminal...terminals!