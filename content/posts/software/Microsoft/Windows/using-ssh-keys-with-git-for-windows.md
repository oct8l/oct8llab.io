---
title: "Using SSH keys with git for Windows"
date: 2018-06-16T14:13:13-05:00
draft: false
fontawesome: true
tags: ["bash"]
categories: ["git for windows"]
keywords: ["ssh","key"]
seoDescription:
summary: 
slug: 
url: 
---

I think everyone would agree that SSH keys are a much better way to authenticate than usernames and passwords. I really just love them. Anyway, I have a freshly reformatted Windows 10 computer that I was wanting to access my GitLab account from, and wanted to set it up with keys for pushing to projects. After installing Git for Windows, I wasn't quite sure how it really worked until a little bit of Googling and of course help from [StackExchange](https://serverfault.com/questions/194567/how-do-i-tell-git-for-windows-where-to-find-my-private-rsa-key) and playing around with some settings.

Ultimately, what you'll want to do is open the Git Bash terminal, which should initially open to your user folder. Run `pwd` to double check. You should get output similar to

```
/c/Users/oct8l
```

# Make the new directory and generate a key

Now, enter `mkdir .ssh` to create the folder to hold your SSH keys. Now `cd` into the new `.ssh` folder, and then enter `ssh-keygen -t rsa -b 4096 -C "$(whoami)@$(hostname)--$(date -I)--SERVICE"` to generate a new SSH key with the comment of `YOURUSERNAME@YOURCOMPUTERNAME--DATE--SERVICE`. Replace `SERVICE` with something like GitLab or GitHub when generating the key to make it easy to tell keys apart. Also enter the same name you put in place of `SERVICE` when prompted for where to save the key. For example, if you enter "gitlab" at the prompt, it will generate a file named `gitlab` and `gitlab.pub` with your private and public SSH keys. Setting a password is optional, but remember that if it's set, you will have to enter the password for the key every time you try to authenticate with it.

If you enter an `ls` command, you should now see your new key.

{{< figure src="/img/posts/2018-167-using-ssh-keys-with-git-for-windows.png" title="" caption="The new SSH public and private keys are now showing" alt="The new SSH public and private keys are created" link="" target="" >}}

# Add your key to your Git service

Now you will want to get the public string from your new key to add to GitLab or GitHub so it knows the key belongs to you. In the Git Bash, enter `cat SERVICE.pub | clip` where `SERVICE` is what you entered when creating the key. This will copy the contents of the public key to your computer's clipboard.

The steps for adding a key to your account vary between Git hosting companies, so check documentation. [^gitlab-add-key] [^github-add-key] [^bitbucket-add-key]

[^gitlab-add-key]: https://docs.gitlab.com/ee/gitlab-basics/create-your-ssh-keys.html
[^github-add-key]: https://help.github.com/articles/adding-a-new-ssh-key-to-your-github-account/
[^bitbucket-add-key]: https://confluence.atlassian.com/bitbucket/set-up-an-ssh-key-728138079.html

# Create an SSH config file

Now if you enter `nano config`, you will be able to write a file to tell your computer which key to use with which service. In `nano`, enter the following:

```sh
Host gitlab.com
    Hostname gitlab.com
    User YOURGITLABUSERNAME
    IdentityFile /c/Users/YOURWINDOWSUSERNAME/.ssh/gitlab
```

{{% alert "blue" %}}

<i class="fa fa-info-circle"></i> **Note:** Make sure to change from `gitlab.com` to `github.com` or `bitbucket.com` and use your respective username for that platform in the "User" field if that is the service you're configuring the SSH key for.

{{% /alert %}} <br>

{{% alert "purple" %}}

<i class="fas fa-exclamation-circle"></i> **Important**: Make sure you use the file name that you entered in the previous step for the `SERVICE` at the end of the "IdentityFile" field.

{{% /alert %}}

Now, try pushing to GitLab or whichever service you defined. If you hadn't set a password, you should just get the output of pushing to the source repository with no errors.

Enjoy!