---
title: "PowerCLI install script"
date: 2018-06-11T11:34:38-05:00
draft: false
fontawesome: false
tags: ["powershell","powercli","script"]
categories: ["vmware","software"]
keywords: ["install"]
seoDescription: I stumbled across a great explanation for installing PowerCLI, and wanted to package it up to make it easy for myself and my team to install.
summary:
slug:
url:
---

Credit for this information should go to [Kyle Ruddy's post](https://blogs.vmware.com/PowerCLI/2018/01/powercli-offline-installation-walkthrough.html) on the VMware Blogs platform. I have just packaged it up into a PowerShell script for ease of deployment.

Currently, this has been tested as of 6/11/2018, and is confirmed working on 64 bit Windows 10 builds 1709 and 1803 (which have PowerShell version 5). PowerCLI is running at 10.1.0 at the current date.

Below is a paste of the script hosted on this site. Feel free to either copy and paste or <a href="{{< base-url >}}scripts/PowerCLI-Install.ps1" download target="_blank">download from the site</a> and execute.

{{< disclaimer >}}<br>

{{< highlight powershell "linenos=table,linenostart=1" >}}

### Variable definitions

$downloadsFolder = [Environment]::GetFolderPath("Desktop")

$psModulesFolder = [Environment]::GetFolderPath("mydocuments")

$powerCliExists = Test-Path -LiteralPath "C:\Program Files (x86)\VMware\Infrastructure\PowerCLI\"

###

### Check for old versions of PowerCLI

If ($powerCliExists -eq $False) {
    Write-Host ""
    Write-Host " No prior versions of PowerCLI detected " -ForegroundColor Black -BackgroundColor Green
    Write-Host ""
}
Else {
    Write-Host ""
    Write-Host " Please remove prior installs of PowerCLI before continuing. Re-run script when this is deleted. " -ForegroundColor Black -BackgroundColor Red
    Write-Host ""
    Exit
}

###

### Grab files for offline install

New-Item -ItemType directory -Path $downloadsFolder\PowerCLI-install >$null

Write-Host ""
Write-Host "Respond with 'y' at the next step when prompted to allow downloading from NuGet. . ."
Write-Host ""

Save-Module -Name VMware.PowerCLI -Path $downloadsFolder\PowerCLI-install

###

### Copy files to modules folder

Copy-Item -Path "$downloadsFolder\PowerCLI-install\*" -Destination "$psModulesFolder\WindowsPowerShell\Modules" -Recurse -Force

###

### Cleanup

Remove-Item -Path "$downloadsFolder\PowerCLI-install" -Recurse -Force

###

###

Write-Host " Installation is complete! " -ForegroundColor Black -BackgroundColor Green
Write-Host " You may need to restart any programs to use the new modules. " -ForegroundColor Black -BackgroundColor Green

###

{{< / highlight >}}

After this is done, wherever your computer's Documents folder is (usually `C:\Users\USERNAME\Documents`), check for a new folder called `WindowsPowerShell`, and the `Modules` subfolder should contain some `VMware.XXX` folders, these should be the PowerCLI modules.

{{< figure src="/img/posts/2018-162-powercli-install-script.png" title="" caption="The PowerCLI modules in the WindowsPowerShell folder" alt="The PowerCLI modules in the WindowsPowerShell folder" link="" target="" >}}

You're now ready to rock with the latest version of PowerCLI!