---
title: "Adding binaries to the Git for Windows PATH"
date: 2018-05-31T14:50:08-05:00
draft: false
toc: false
fontawesome: false
tags: ["bash"]
categories: ["Git for Windows"]
keywords: ["binaries","path","windows"]
seoDescription: "I wanted to be able to add some binaries to my PATH in the Git for Windows terminal. Here's how I do it."
summary: 
---

As WSL becomes more mature, I will surely start using it more than I use the Bash that comes installed with Git for Windows. But for now, my main Bash interface is through the Git Bash terminal.

On my Windows computers for work (I don't have much of a choice), I have a few scripts I like to port from my personal setup that I use frequently. One of the more relative ones that I figured I'd use as an example is my script to generate checksums for the scripts I upload. The list of these files can be found at <a href="{{< base-url >}}scripts"  target="_blank">/scripts</a>.

An easy way to get this script in my PATH would be to enter `curl {{< base-url >}}scripts/checksums.sh -o "/c/Program Files/Git/mingw64/checksums.sh"`.

This should download the file straight to my Git for Windows Bash PATH for generating checksums.

Now I'm good to go to run `checksums.sh checksums.sh` or similar!