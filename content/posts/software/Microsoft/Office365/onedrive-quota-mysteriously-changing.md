---
title: "OneDrive Quota Mysteriously Changing"
date: 2018-04-02T20:33:40-05:00
draft: false
toc: false
keywords: []
seoDescription:
summary:
slug:
tags: ["office 365", "onedrive"]
categories: ["microsoft", "software"]
url:
---

Recently, we had a situation at work where seemingly random employees were experiencing their OneDrive capacity going down to 2GB (as opposed to the 1TB everyone should have), and they were receiving errors on their desktop clients when trying to sync files from their computers, as they were all well past the 2GB limit.

We had to open a PowerShell instance, run [Connect-SPOService](https://docs.microsoft.com/en-us/powershell/module/sharepoint-online/connect-sposervice?view=sharepoint-ps) and reset the user's account to have the 1TB of storage that they should.

At first, it seemed like only one person was affected, and they were fixed up after we ran the PowerShell commands.

But then, a second user let us know that it was happening to them. And then our original user had the issue pop up again. And before we knew it, we had six employees who were all consistently having issues with their OneDrive capacity.

It was very odd since we hadn't implemented any major changes in our infrastructure or licensing, so there wasn't much of an explanation. But then after a little thinking, we realized we had recently added Visio Online to our Microsoft agreement. We happened to have assigned licenses to...**six** employees.

We headed into our Office 365 admin portal and checked out what was different with these six employees besides having Visio Online. Everything looked the same between their accounts and the rest of our users' accounts. So we figured that under the Visio entry might be something of interest, and that is when we found what we were looking for.

In the screenshot below, you will see the sliders that all should be turned on, because more options are better, right? In this case, no.

When the slider for OneDrive for Business Basic is turned on, it will fight with the correct allotment and keep trying to change it to 2GB.

<center><img src="/img/posts/2018-92-onedrive-quota-mysteriously-changing.png" width="60%" height="60%"/></center>

After turning off the slider, we had to run the [SPO PowerShell commands](https://support.office.com/en-us/article/change-your-users-onedrive-storage-space-using-powershell-7448173d-a38c-48cf-acbb-09ac1b6237d4) to get it back to 1TB and then afterwards we stopped running into this issue.

Hope this can help someone out who is struggling with the same issue!
