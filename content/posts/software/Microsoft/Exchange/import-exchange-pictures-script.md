---
title: "Import Exchange photos script"
date: 2018-08-02T13:23:50-05:00

draft: false

toc: false

fontawesome: false

tags: ["powershell","exchange","script"]
categories: ["microsoft"]
keywords: ["photo","set-userphoto"]
seoDescription:

summary:

slug:

url:

### optional cover photo params ###
covertitle: false
cover: /img/posts/covers/2018-214-powershell-prompt.png
coveropacity: 30%
covertext:
coverheight: 20vh
---

This script is useful for setting multiple Exchange photos at once. In our still on-prem deployment, this is probably the quickest way for us to set pictures in batches when we have new hires.

This script is an amalgamation of a few different scripts I use in production, but _mostly_ based off of a [script from user Richard Diphoorn](https://powershell.org/forums/topic/how-to-import-user-photos-in-bulk-via-ps-and-exchange-online/#post-37604) at powershell.org forums.

My specific iteration of the task that works for our environment is copying the photo to the local user's `pictures` directory, and then running this script to pull those files in. The key is to have the file name be the user's AD username, for example Rob Smith's picture would be named `rsmith.jpg` and live in the current user's `Pictures` folder.

{{< disclaimer >}}<br>

{{< highlight powershell "linenos=table,linenostart=1" >}}

$admUser = "$env:USERNAME"
$imageFolder = "C:\Users\$admUser\Pictures"
$pictures = Get-ChildItem $imageFolder

foreach ($picture in $pictures){

    Try {
        $user = Get-User -Identity $($picture.BaseName) -ErrorAction Stop
        }

    Catch {
        Write-Warning "Warning: $_"
    }

    If ($user) {
        $user | Set-UserPhoto -PictureData ([System.IO.File]::ReadAllBytes("$imageFolder\$($picture.Name)")) -Confirm:$false
    }
}

{{< / highlight >}}

As long as the picture is around ~250kb or less, it is lightening fast and can set a couple hundred pictures in just about two minutes by my tests.

Happy `Set-UserPhoto`ing!