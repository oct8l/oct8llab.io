---
title: "Relaxed Theme for Royal TS"
date: 2019-01-08T15:28:14-06:00

draft: false

toc: false

fontawesome: false

ogVideo:
ogAudio:
ogImage:

tags: ["relaxed theme","royal ts"]
categories: ["software"]
keywords: ["theme","colors"]
seoDescription:

summary:

slug:

url:

### optional cover photo params ###
covertitle: true
cover: /img/posts/covers/2019-8-royal-ts-relaxed-theme.png
coveropacity: 40%
covertext:
coverheight: 21vh
---

In my conquest to make everything have the fantastic [Relaxed Theme](https://github.com/Relaxed-Theme), I have translated the colors into Royal TS 4 and 5 (They are the same colors and layouts for both versions).

Below are screenshots and pastes of the hex codes:

![Normal color codes](/img/posts/2019-8-royal-ts-relaxed-theme/normal.png)<br><br>

| Colors | Hex code |
|:---------:|:----------:|
| Default foreground | #D9D9D9 |
| Cursor Text | #D9D9D9 |
| Black | #353A44 |
| Green | #909D63 |
| Blue | #6A8799 |
| Cyan | #C9DFFF |
| Default background | #353A44 |
| Cursor color | #D9D9D9 |
| Red | #BC5653 |
| Yellow | #EBDA7A |
| Magenta | #B06698 |
| White | #F7F7F7 |

<br><br>

![Bright color codes](/img/posts/2019-8-royal-ts-relaxed-theme/bright.png)<br><br>


| Colors | Hex code |
|:---------:|:----------:|
| Default foreground | #D9D9D9 |
| Black | #7F7F7F |
| Green | #C9DA8B |
| Blue | #9AC1DA |
| Cyan | #E3EEFF |
| Default background | #353A44 |
| Red | #DB7E7B |
| Yellow | #F3E7A5 |
| Magenta | #E9A5D3 |
| White | #F7F7F7 |

<br>

Happy Relaxing!