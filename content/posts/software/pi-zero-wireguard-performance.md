---
title: "WireGuard performance with a Pi Zero"
date: 2019-05-20T14:52:16-05:00

draft: false

toc: false

fontawesome: false

ogVideo:
ogAudio:
ogImage:

tags: ["wireguard","linux","raspberry pi"]
categories: ["wireguard"]
keywords: ["vpn","pi zero"]
seoDescription: I haven't been able to find many Pi Zero benchmarks with using Wireguard, so I thought I would take a shot at compiling some of my own.

summary: I had a spare Pi Zero (version 1.3 used for these tests) laying around, and I thought I’d try and check out what some basic performance marks were when running the Pi Zero as a WireGuard server. I haven’t been able to find a whole lot of other benchmarks for running WireGuard on a Pi Zero, so I thought I’d try some out.

slug:

url:

### optional cover photo params ###
covertitle: false
cover: /img/posts/covers/harrison-broadbent-1459951-unsplash.jpg
coveropacity: 30%
covertext:
coverheight: 30vh
---

I had a spare Pi Zero (version 1.3 used for these tests) laying around, and I thought I'd try and check out what some basic performance marks were when running the Pi Zero as a WireGuard server. I haven't been able to find a whole lot of other benchmarks for running WireGuard on a Pi Zero, so I thought I'd try some out. As a side note, the USB ethernet adapter I'm using is [this one from Smays](https://smile.amazon.com/Smays-Ethernet-compatible-Raspbian-Raspberry/dp/B00L32UUJK).

My client in this situation was a VM running Debian with 2 CPU cores and 2GB of RAM, so it should be more than powerful enough to run WireGuard and not be a bottleneck.

My Raspberry Pi Zero is running Raspbian Lite (April 2019 version). I know that there are other spins like DietPi that could allow it to perform faster, but I used Raspbian for this test.

{{% alert "blue" %}}

<i class="fa fa-info-circle"></i>&nbsp;**Note:** in the Asciicasts below, some have two panes split horizontally. This is me using tmux to access the Debian VM on top and the Pi Zero on bottom to keep an eye on CPU utilization while performing these tests.

{{% /alert %}} <br>

# The tests

To start, here is a baseline of running iperf from my Debian VM to the Pi Zero without WireGuard:

<center>{{< asciicast 1PHSODAHed6V3lCWfoXQCzhHV >}}</center>

As expected, the speed is around 90 megabits per second, as the Pi Zero has a USB 2.0 OTG port, and I'm using a 100mb ethernet adapter for it.

Now I'll head to my Pi and turn WireGuard on as a server, and enable it on my Debian VM and try iperf again:

<center>{{< asciicast aGKT6rsFmJC4tKkNyVTdu5nOG >}}</center>

At 3 seconds into the Asciicast, we can see that I have ALL IPv4 traffic set to route over WireGuard (0.0.0.0/0). With this test running over WireGuard, we can see that the bi-directional test (`-r` flag with iperf) is not fantastic, but depending on the use case for a Pi Zero WireGuard server, it could get the job done with ~30-40 megabits per second speed capabilities.

While using the bi-directional simultaneous test (`-d` flag with iperf), the speed drops to ~20 megabits per second. Again, depending on the use case, may be enough throughput. Heck, even some rural towns in the US aren't able to get these speeds in the first place!

Now for some internet speed tests for fun, I downloaded the [speedtest-cli](https://github.com/sivel/speedtest-cli) binary and ran it both with and without WireGuard from my Debian VM, and also directly from the Pi Zero (both are running Python 2.7.13).

Here are the results from the test with the Pi Zero straight to the internet:

<center>{{< asciicast PmWHYiP8WbzIs5WsgzokmKJu7 >}}</center>

This gave us a baseline of 53 down by 28 up without anything else going on. Not the greatest, but since these numbers are lower than it was with a straight iperf test, I think there's some performance loss with the script using Python.

Nonetheless, here is a test from my Debian VM without WireGuard running:

<center>{{< asciicast WuRyyzI9vQtuUfCEAVG25XFdW >}}</center>

And a test _with_ WireGuard running:

<center>{{< asciicast FIkg79h7Wo67bfVTHwn7of6Jn >}}</center>

This shows a speed of around 21 down by 19 up with WireGuard running and accessing the internet over the WireGuard server on the Pi Zero.

# Closing

Those are all of my tests for now. In closing, I'd say that if you're planning on using WireGuard on an iOS device with the On-Demand Activation for untrusted wi-fi networks when away from the house, this should get the job done to protect you on public wi-fi networks. If the goal is permanent, high throughput usage, I would recommend a more powerful box to run WireGuard.