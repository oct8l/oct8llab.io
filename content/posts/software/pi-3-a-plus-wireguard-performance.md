---
title: "WireGuard performance with a Pi 3 A+"
date: 2019-05-21T10:16:00-05:00

draft: false

toc: false

fontawesome: false

ogVideo:
ogAudio:
ogImage:

tags: ["wireguard","linux","raspberry pi"]
categories: ["wireguard"]
keywords: ["vpn","pi","3 a plus","3a+"]
seoDescription: After benchmarking WireGuard on the Pi Zero, I thought I'd do similar tests with another Pi 3A+ I had on hand.

summary: After running the WireGuard benchmarks on a Pi Zero, I thought I'd get out my Raspberry Pi 3A+ and try it out just for fun.

slug:

url:

### optional cover photo params ###
covertitle: false
cover: /img/posts/covers/harrison-broadbent-1459952-unsplash.jpg
coveropacity: 30%
covertext:
coverheight: 30vh
---

After running the [WireGuard benchmarks on a Pi Zero](/posts/2019/140/wireguard-performance-with-a-pi-zero/), I thought I'd get out my Raspberry Pi 3A+ and try it out just for fun. These were performed with this [USB-to-ethernet adapter from UGreen](https://smile.amazon.com/dp/B00LLUEJXW)

As with the other test, my client was a VM running Debian with 2 CPU cores and 2GB of RAM as to not be a bottleneck in this test.

My Raspberry Pi 3A+ is running Raspbian Lite (April 2019 version) rather than something like DietPi or Armbian.

{{% alert "blue" %}}

<i class="fa fa-info-circle"></i>&nbsp;**Note:** in the Asciicasts below, some have two panes split horizontally. This is me using tmux to access the Debian VM on top and the Pi Zero on bottom to keep an eye on CPU utilization while performing these tests.

{{% /alert %}} <br>

# The tests

To start, here is a baseline of running iperf from my Debian VM to the Pi Zero without WireGuard:

<center>{{< asciicast V8ffIhoWAvDfcEytOJjAGy3II >}}</center>

This speed is pretty much wire speed for a 100mb USB NIC.

I'll now turn on WireGuard and try iperf again:

<center>{{< asciicast mTyw5n8HbjAsW2fweYDKZWjaS >}}</center>

It looks like the results are still near wire speed, and the CPU utilization is fairly even across all four CPU cores when running WireGuard.

I'll now run the same [speedtest-cli](https://github.com/sivel/speedtest-cli) tests that I did in the Pi Zero benchmark, testing first from the Pi to the internet:

<center>{{< asciicast QwUM0SMkWnB5F27m8j1NnanER >}}</center>

Again, near wire speed in this test.

Now I'll paste the Asciicast from the other test, just because I don't feel a need to replicate the test 🙃:

<center>{{< asciicast WuRyyzI9vQtuUfCEAVG25XFdW >}}</center>

And now a few tests from the Debian VM running over WireGuard (while setting all IPv4 traffic to flow over WireGuard):

<center>{{< asciicast epSdWuzfb0Xvwvwzl6I1RnH1u >}}</center>

# Closing

As you can see, the speeds vary from 46 to 80 mbps download, and 20 to 60 mbps upload. Either way, this Pi is definitely more capable than the Pi Zero to handle WireGuard traffic. With the extra headroom, you could even run other services such as an Nginx reverse proxy or other similar services if you decide to run this at the DMZ edge of your home network, or as a simple server in any other type of network.