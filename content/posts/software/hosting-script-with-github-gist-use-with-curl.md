---
title: "Hosting a cURLable script with GitHub Gists"
date: 2019-03-18T09:29:26-05:00

draft: false

toc: false

fontawesome: false

tags: ["github","linux","curl","bash"]
categories: ["software"]
keywords: ["curl","pipe","host script","curl script","curl bash script"]
seoDescription:

summary:

slug:

url:
---

After making some entirely too long BASH scripts, I like to host them somewhere online to make it easier to get the script to whatever device I'm using. This is especially true when I want to run a long script on a brand new machine, a machine that I might not have enabled secure SSH access on yet, but one where I still have access to a tty. This is where the handy [GitHub Gists](https://gist.github.com/) comes in to play. GitHub has made it easy to share basic text files, à la Pastebin, but cooler.

As an aside, I'd like to mention that this isn't the only way to accomplish having a `cURL`able file out on the internet to make accomplishing this task easy. It's just one that I find to be simple enough, and GitHub is providing a free CDN with Gists to use for this. If you have your own domain, you can even set up a 301 redirect and avoid needing to use a public shortening service. If you have your own website, you could also use that to host a plain text file and avoid using GitHub Gists. The possibilities are endless!

# Creating a Gist
If you don't already have a GitHub account, you'll want to go and set one up. After that, you'll want to head to [GitHub Gists](https://gist.github.com/) and allow it to sign in using your GitHub account. You can now post your first Gist!

After being authenticated, click the **New gist** button in the top right to be taken to the form for posting a new Gist. Fill out your description (if you'd like), the filename, and then you can enter your script into the main form. Below, I have a script that says hello, and a friendly warning meant for if you have piped it straight to BASH (which can be very dangerous!). Afterwards, you can choose to post it either publicly or create a "secret" Gist. Creating a secret Gist will only make it accessible from those who have a link, but it won't make it visible on your profile or to search engines.

![Gist screenshot](/img/posts/2019-77-hosting-a-curlable-script-with-github-gists/1.png#center)

# Getting the link
After you have your Gist posted, you'll be taken to your Gist's page. It will have a comment section just like a GitHub repo, and you will have a few buttons. What you'll want to do next is click the **Raw** button in the top right of your Gist, and that will take you to a `cURL` friendly version of your Gist.

For example, when I click the **Raw** button on [my Gist](https://gist.github.com/oct8l/99e3366923697a491afb9931fbb3e504), I get taken to a URL of `https://gist.githubusercontent.com/oct8l/99e3366923697a491afb9931fbb3e504/raw/85618c36cedcfcea2af214c63500f94c7cb9604e/hello.sh`. From this, we can derive the permanent URL of _this_ Gist that we can use. The key will be to delete everything after the word `raw` in the Gist's URL. In my case, that would leave me with `https://gist.githubusercontent.com/oct8l/99e3366923697a491afb9931fbb3e504/raw`.

What we're doing is taking off the file name of `hello.sh` and the commit hash of `85618c36cedcfcea2af214c63500f94c7cb9604e`. Essentially, it's the equivalent of getting the link to the `master` branch of the Gist, if that makes sense. Now you can edit the Gist through the web interface, or even clone the Gist just like a regular git repo. No matter what, this Gist will always have the same ID, such as `99e3366923697a491afb9931fbb3e504` in my case.

# Shortening the link
Now once you have derived the link to the "master" of your Gist, head on over to a service like [Bitly](https://bitly.com) or [TinyURL](http://tinyurl.com/) and get a shortened version. This is much nicer to type in at a terminal than the full Gist URL. For this, I'll use TinyURL, and enter my Gist URL of `https://gist.githubusercontent.com/oct8l/99e3366923697a491afb9931fbb3e504/raw`. I'm given the output of `http://tinyurl.com/y4zwnz7b` which I can then use from a terminal and pull down the script using `cURL`.

![TunyURL created](/img/posts/2019-77-hosting-a-curlable-script-with-github-gists/2.png#center)

# Using with `cURL`
The easiest way to do this is one that I've stolen from the [Pi-hole project](https://pi-hole.net/). We'll use `cURL` with the [-s](https://curl.haxx.se/docs/manpage.html#-s), [-S](https://curl.haxx.se/docs/manpage.html#-S) and [-L](https://curl.haxx.se/docs/manpage.html#-L) flags, which will make `cURL` run silently (with no progress bar), follow redirects (since we're using a URL shortening service), and to show errors if there are any when executing `cURL`. With all of these flags together, we currently have `curl -sSL`, and with our new shortened link, we have `curl -sSL http://tinyurl.com/y4zwnz7b`. Now to get that into BASH, we will use the pipe (|) character, and altogether, we can enter this into our terminal: `curl -sSL http://tinyurl.com/y4zwnz7b | bash`.

![Running the script](/img/posts/2019-77-hosting-a-curlable-script-with-github-gists/3.gif#center)

Depending on our script, it might be required to pipe to `sudo bash` instead of just `bash`. If you'll be performing any administrative tasks as a user who has `sudo` permissions, but not root, you'll need to use `sudo bash`.

Happy `cURL`ing!