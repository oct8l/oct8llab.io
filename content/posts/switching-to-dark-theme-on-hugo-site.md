---
title: "Switching to Dark Theme on Hugo Site"
date: 2021-11-03T00:43:22-05:00

draft: true

toc: false

fontawesome: false

ogVideo:
ogAudio:
ogImage:

tags: []
categories: []
keywords: []
seoDescription:

summary:

slug:

url:

### optional cover photo params ###
covertitle: false
cover:
coveropacity: 30%
covertext:
coverheight: 20vh
---

