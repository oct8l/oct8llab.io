---
title: "Figuring out VLANs on the Zyxel GS1900-24"
date: 2018-05-20T13:39:05-05:00
draft: false
toc: false
tags: ["networking","vlans"]
categories: ["home-networking"]
keywords: ["xyzel","switch"]
seoDescription: The Zyxel GS1900-24 has a bit of a unique interface for setting VLANs. I took some time to try to clarify setting these up.
summary:
slug:
url:

### optional cover photo params ###
covertitle: true
cover: "/img/posts/covers/pexels-photo-442150.jpeg"
coveropacity: 50%
coverheight: 20vh
---

Every great network needs a great core switch, and I've found the [Zyxel GS1900-24](https://www.zyxel.com/us/en/products_services/8-10-16-24-48-port-GbE-Smart-Managed-Switch-GS1900-Series/) to be perfect for my home's needs. When I moved from DD-WRT on a WNDR3000 to a [PC Engines APU2C4](https://www.pcengines.ch/apu2c4.htm) running pfSense, I had to move the switching capabilities to an external device. This sent me out on a search for a new managed switch with gigabit networking capabilities and preferably 24 or more RJ45 ports. I also was wanting extra ports for hooking up my two Dell R710 servers with the SFP networking ports they had. As it turns out, the Zyxel switch was exactly what I was looking for, and had a very reasonable price when purchased used on eBay.

# The scenario

I've recently been wanting to get my network segmented out as the number of smart TVs, e-readers and other network-enabled devices grows in my household. This led me down the path of segmenting my network into different VLANs through pfSense, and consequently, through the GS1900-24. Since this switch has a GUI for management, it took a little bit to translate my previously somewhat limited understanding of VLANs into how this switch wanted to be set up. I had a bit of difficulty finding resources for this specific switch when searching around the internet, so I thought I would write something up here to possibly help someone else out.

For reference, I am doing all of this on [firmware V2.40(AAHL.1)](https://www.zyxel.com/us/en/support/download_landing/product/gs1900_series_12.shtml?c=us&l=en&pid=20130521174809&tab=Firmware&pname=GS1900%20Series), which is the most up-to-date version available as of writing.

# Switch configuration

Load up the web UI for your switch and head to **Configuration > VLAN > VLAN**

![VLAN config page](/img/posts/140-figuring-out-vlans-on-the-zyxel-gs1900-24/1.png)

As shown above, I currently have four VLANs set up:

|  VLAN  |  Purpose          |
|:------:|:------------------|
| 1      | Default           |
| 44     | Steam streaming   |
| 55     | Media devices     |
| 66     | Guest wifi        |

The VLAN I'll focus on in this section will be VLAN 55, the VLAN that my Roku smart TV, Tablo streaming box, Amazon Fire TV Stick are on. This VLAN is built out in pfSense to only have access to the internet, and to allow the Tablo box talk out to the other VLANs so the streaming will still work on other devices, e.g. my desktop. This VLAN is also built out through the Ubiquiti AP-AC-Lite for wireless clients like my Roku sticks and Fire TV Stick.

## Tagging VLANs

Now is the fun part, tagging our VLANs. This assumes you've already gone through and clicked the **Add** button to fill in the VLAN number under _VLAN List_ and the name under _VLAN Name Prefix_.

At the top right of the chart, click the **VLAN Port** button to get to the interface when we can set up how VLANs are going to work with tagged and untagged traffic.

![VLAN port button](/img/posts/140-figuring-out-vlans-on-the-zyxel-gs1900-24/2.png)

On my switch I have the following key devices plugged into my switch:

| Port |  Device         |
|:----:|:----------------|
| 12   | pfSense router  |
| 21   | AP-AC-Lite      |
| 22   | Tablo           |

In the top drop down box, go to the VLAN you'd like to configure ports for, such as 55 for me. It should look similar to the picture below, with all ports tagged as **Excluded**.

![VLAN excluded](/img/posts/140-figuring-out-vlans-on-the-zyxel-gs1900-24/3.png)

I will configure the ports so port 12 (pfSense) will now allow **Tagged** traffic (for routing out to the internet, DHCP, DNS, etc), port 21 (AP-AC-Lite) will allow **Tagged** traffic (for talking between a streaming stick and pfSense), and port 22 (Tablo) will allow **Untagged** traffic (for talking to other devices on the VLAN for streaming OTA programming).

This is the result of tagging my ports:

<a name="tagged-vlan-55">![Configured VLANs](/img/posts/140-figuring-out-vlans-on-the-zyxel-gs1900-24/4.png)</a>

Now you might be wondering why the Tablo port will allow untagged traffic if I'm going to want it to be on the VLAN 55 network. To answer this, we'll head over to the **Port** section.

![Port button](/img/posts/140-figuring-out-vlans-on-the-zyxel-gs1900-24/5.png)

You will notice below that port 22 (Tablo) is showing its PVID as 55:

![Port PVID](/img/posts/140-figuring-out-vlans-on-the-zyxel-gs1900-24/6.png)

I have configured this by checking the box to the left of the port number, and clicking the **Edit** button at the bottom of the page. This will tak you to a page where you can configure specific information about the port, including its PVID. The PVID is essentially what traffic will be tagged as if the device itself leaves it untagged. So since the Tablo is fairly basic in its network configuration and doesn't have an option to specify a VLAN, it isn't tagging any frames coming out of the device, the switch will run the untagged frames over VLAN 55 by default.

![Port config](/img/posts/140-figuring-out-vlans-on-the-zyxel-gs1900-24/7.png)

This enables us to have any network device, whether it is aware of the concept of VLANs or not, be routed over a chosen VLAN. If you refer back to [how the ports were tagged](#tagged-vlan-55) earlier, it will make sense that port 22 (Tablo) would be set to **Untagged**, so the traffic would flow over VLAN 55. As a general rule, you won't want to make any port **Untagged** on more than one VLAN, so head back to VLAN 1 via the drop down and mark that port as **Excluded**.

## Save your config!

As always, don't forget to save your running config! On this switch you can hit the **Save** button in the top right.

![Save button](/img/posts/140-figuring-out-vlans-on-the-zyxel-gs1900-24/8.png)

Hopefully this will help someone out who was also struggling to figure this out!