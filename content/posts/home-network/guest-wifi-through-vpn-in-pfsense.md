---
title: "Routing the guest wifi through OpenVPN in pfSense"
date: 2018-05-14T12:29:36-05:00
draft: false
toc: false
tags: ["pfsense","ubiquiti","networking"]
categories: ["home-networking"]
keywords: ["unifi","pia","vpn","guest wifi","wifi","guest","home"]
seoDescription: "I wanted to separate my guest wifi by VLAN from my network for security purposes, and then route them out to the internet through a VPN."
summary: 
slug: guest-wifi-through-vpn-pfsense
url: 
---

When I originally bought my Unifi AP AC Lite, I hadn't put much thought into guest access. I was mainly getting it to improve my own network connectivity, and to get an AP that was independent of my router for greater flexibility. At first, I was using DD-WRT as my core router and firewall, but I have since moved to pfSense on an [PC Engines APU2C4](https://www.pcengines.ch/apu2c4.htm) which provides even more features than DD-WRT.

While the guest segregation option in the Unifi Controller works well, I was still wanting to give guests their own DHCP range and more importantly, route all outgoing traffic out to the world through a VPN. This is mainly protection for myself, as it doesn't expose my IP address out to the internet for tracking or for possibly questionable activities that guests might be up to.

The first step was to create a new VLAN in pfSense to get the wifi network from the AP to the router segregated from my regular network traffic. I found some great instructions at [RedPacket Security](https://www.redpacketsecurity.com/ubiquiti-guest-wifi-vlans-and-pfsense/) that walked through this step by step with great screenshots (JavaScript required for image viewing).

The next step after getting this to work was to get my new VLAN routing over a VPN connection that is terminated inside of pfSense. An article has been written on [kroy.io](https://blog.kroy.io/vpn-routing-and-pfsense-piecemeal-style/) that will be better and more detailed than anything I could do.

After combining these two processes, I now have a wireless network for guests that is completely segregated from my home network and goes out to the world through a PIA VPN. The APU2C4 is a very low powered device, so speeds aren't as high as if it were a full blown computer, but they will be more than adequate when throttling guests through the Unifi Controller.

<center><img src="/img/posts/2018-132-guest-wifi-vpn.png" width="30%" height="30%" alt="Speed test while on VPN" /></center>