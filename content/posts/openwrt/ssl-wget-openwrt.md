---
title: "Allowing SSL in wget on OpenWrt/LEDE"
date: 2018-04-04T20:47:22-05:00
draft: false
toc:
tags: ["ar300m","wget","script"]
categories: ["openwrt"]
keywords: ["gl-inet"]
fontawesome: true
seoDescription: A guide on allowing wget use SSL/TLS. I use this for downloading additional blocklists from sources such as github, which uses HTTPS.
summary:
slug: allow-ssl-in-wget-openwrt
url:
---

# Why?
For the [post regarding blocking ads in OpenWrt](/posts/2018/100/blocking-ads-in-openwrt/), I had to figure out how to add some extra blocklists, including those from [EasyList](https://easylist.to/) and [AdAway](https://adaway.org/). In vanilla OpenWrt, we'll have to use `wget` to retrieve files, and I like to make sure that I can use secure protocols when grabbing text files across the internet.

# The scripts

## `ssl-in-wget.sh`

To enable using SSL/TLS in `wget`, make a script called `ssl-in-wget.sh` with the following:

{{< highlight sh "linenos=table,linenostart=1" >}}

#!/bin/sh

# Information from openwrt.org wiki: https://wiki.openwrt.org/doc/howto/wget-ssl-certs
#
# Script written by oct8l (www.oct8l.com)
#
# Script licensed as CC BY-NC-SA 3.0 (https://creativecommons.org/licenses/by-nc-sa/3.0)

# Make the directory for the SSL certs
mkdir -p /etc/ssl/certs
# Set the directory so wget knows where to look
export SSL_CERT_DIR=/etc/ssl/certs
# Reload the shell after the last step
source /etc/profile
#Update packages and install the newest version of wget along with certificates and utilities for SSL
opkg update
opkg install wget
opkg install openssl-util
#opkg install ca-certificates # Uncomment this line if you're running LEDE 17.01.4 or later
#opkg install ca-bundle # Uncomment this line if you're running LEDE 17.01.4 or later
#opkg install libustream-openssl # Uncomment this line if you're running LEDE 17.01.4 or later
echo
echo "--------------------------------"
echo "You can now wget from https URLs"
echo "--------------------------------"

{{< / highlight >}}<span class="brsmall"></span>

<sup class="code">[From the OpenWrt wiki](https://wiki.openwrt.org/doc/howto/wget-ssl-certs#install_wget_with_ssl)</sup>

<sup class="code">Grab from ash with `wget {{< friendly-base-url >}}/scripts/ssl-in-wget.sh`<br>Make sure to check against the checksums at <a href="https://{{< friendly-base-url >}}/scripts/ssl-in-wget.sh_checksum.txt" target="_blank">https://{{< friendly-base-url >}}/scripts/ssl-in-wget.sh_checksum.txt</a></sup>

{{% alert "purple" %}}

<i class="fas fa-exclamation-circle"></i>&nbsp; **Notice:** Make sure you uncomment the first hashes in lines 19-21 if you're running LEDE, or firmware version `2.27` or newer on the AR300M

{{% /alert %}}

Now you should have most major CA certificates installed and trusted on your router. This includes the certificates used for adaway.org and easylist.to at the time of writing.

{{% alert "blue" %}}

<i class="fa fa-info-circle"></i>&nbsp;**Note:** You can stop here unless you have an issue with a specific site's certificate

{{% /alert %}}

If you attempt to use wget and get an error about the SSL certificates for a site not being trusted, you can use the script below to manually add a .cer file. If you need to retreive the .cer file, there are instructions at the [OpenWrt wiki](https://oldwiki.archive.openwrt.org/doc/howto/wget-ssl-certs#adding_certificates_manually).

## `add-cert.sh`

After you have the `.cer` file, create a script named `add-cert.sh` with the following:

{{< highlight sh "linenos=table,linenostart=1" >}}

#!/bin/sh
# author: joda
openssl=/usr/bin/openssl
certdir=$SSL_CERT_DIR
if [ ! -f $openssl ]; then
  echo "ERROR: Can't find $openssl. openssl-util installed?" >&2
fi
if [[ "$1" = "-f" ]]; then
   overwrite=1
   shift # remove $1
fi

if [ -f "$1" ]; then
  certfile=$1
  certname=`basename $certfile`
  echo "Certificate $certname"
  echo "  copy to $certdir"
  if [ "1" -ne "$overwrite" ] && [ -f "$certdir/$certname" ]; then
    echo >&2
    echo "ERROR: certificate $certname exists" >&2
    exit 2;
  fi
  cp "$1" "$certdir/$certname"

  # create symbolic link from hash
  echo -n "  generating hash: "
  HASH=`$openssl x509 -hash -noout -in $certfile`
  echo "$HASH"

  # handle hash collisions
  suffix=0
  while [ "1" -ne "$overwrite" ] && [ -h "$certdir/$HASH.$suffix" ]; do
    let "suffix += 1"
  done
  echo "  linking $HASH.$suffix -> $certname"
  if [ $overwrite ]; then
    ln -sf "$certname" "$certdir/$HASH.$suffix"
  else
    ln -s "$certname" "$certdir/$HASH.$suffix"
  fi
else
  echo >&2
  echo "ERROR: file does not exist $1" >&2
  echo >&2
  echo "This script adds (root) certificates for wget(ssl) to $certdir." >&2
  echo "SYNTAX: `basename $0` [Options] [x509-certificate]" >&2
  echo >&2
  echo "Option: -f      force overwriting if certificate exists" >&2
fi

{{< / highlight >}}<span class="brsmall"></span>

<sup class="code">[From the OpenWrt wiki](https://wiki.openwrt.org/doc/howto/wget-ssl-certs#adding_certificates_manually)</sup>

<sup class="code">Grab from ash with `wget https://gitlab.com/oct8l/openwrt-adblock/raw/ed327c55/addcert.sh`</sup>

Now you should be all set to use SSL inside of `wget`!
