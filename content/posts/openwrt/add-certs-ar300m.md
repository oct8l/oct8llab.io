---
title: "Add SSL cert to LuCI on the AR300M"
date: 2018-04-09T20:47:14-05:00
draft: true
toc:
tags: ["ar300m","ssl cert","script", "luci"]
categories: ["openwrt"]
keywords: ["gl-inet"]
seoDescription:
summary:
slug: openwrt-luci-add-ssl-cert
url:
---



This seems to work when visiting LuCI, but not on the GL-iNet interface accessed at the root of the device.

# The script

This may work on other OpenWrt routers, but it was crafted for and tested on a [GL-iNet AR300M](https://www.gl-inet.com/ar300m/).

{{< highlight sh "linenos=table,linenostart=1" >}}

#!/bin/sh

# script from http://oct8l.com
# inspired by https://fabianlee.org/2016/09/19/openwrt-enabling-https-for-the-luci-web-admin-interface/

intaddr=$(ifconfig br-lan| grep 'inet addr:' | cut -d: -f2 | awk '{ print $1}')

opkg update
opkg install luci-lib-px5g px5g-standalone libustream-openssl
opkg install luci
echo
echo "----------------------------"
echo "Packages have been installed"
echo "----------------------------"
/etc/init.d/uhttpd restart
echo "----------------"
echo "uhttpd restarted"
echo "----------------"
echo
echo "Please visit https://$intaddr/cgi-bin/luci in your browser"

{{< / highlight >}}

Run this script from ash on your OpenWrt router, and you should now have an SSL cert! Just trust it from your browser or computer and you should now have secure access.