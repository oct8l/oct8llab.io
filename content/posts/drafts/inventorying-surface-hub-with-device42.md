---
title: "Inventorying Surface Hub With Device42 ADS"
date: 2018-06-14T13:35:50-05:00
draft: true
toc: false
fontawesome: false
tags: ["device42","surface hub"]
categories: ["microsoft"]
keywords: ["ads","discovery"]
seoDescription: "We have implemented Device42 with the auto discovery service, but we weren't seeing our Surface Hubs for inventorying. Now we do!"
summary: 
slug: surface-hub-device42-ads
url: 
---

