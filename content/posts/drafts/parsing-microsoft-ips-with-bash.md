---
title: "Parsing Microsoft Office 365 IPs with BASH scripts"
date: 2019-02-22T13:45:45-06:00

draft: true

toc: false

fontawesome: false

ogVideo:
ogAudio:
ogImage:

tags: ["exchange online"]
categories: ["Microsoft"]
keywords: []
seoDescription:

summary:

slug:

url:

### optional cover photo params ###
covertitle: false
cover:
coveropacity: 30%
covertext:
coverheight: 20vh
---

https://stackoverflow.com/questions/36839706/parsing-json-from-shell-script-using-json-sh
https://stackoverflow.com/questions/20762575/explanation-of-convertor-of-cidr-to-netmask-in-linux-shell-netmask2cdir-and-cdir
https://www.unix.com/shell-programming-and-scripting/280541-convert-ip-ranges-cidr-netblock.html