---
title: "An Example Post"

date: 2018-04-06T19:40:58-05:00

draft: true

tags: ["testTag","exampleTag"]
categories: ["testCat","exampleCat","wow"]
keywords: ["testKey","exampleKey"]

toc: true

fontawesome: true

description: A post to show all features that I've added so far so I don't forget

summary: 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123 123

url:

### optional cover photo params ###
covertitle: false
cover: "/img/posts/pexels-photo-691571.jpeg"
coveropacity: 30%
covertext: "Secondary text123"

comments:
---

`sourced from https://en.wikipedia.org/wiki/OpenNMS`

<center><i class="fas fa-robot" style="font-size: 20em"></i></center>
<br>
<center><i class="fab fa-creative-commons" style="font-size: 35em"></i></center>

Monitoring
: Watching something to make sure it's always working

{{% alert "green" %}}

<i class="fas fa-check-square"></i>&nbsp;**Tip**: Interface ::1

Recently had a nasty experience with upgrading IPA server from 4.3 to 4.5. Apparently the new ipa-server-upgrade does a check and if ::1 exists in the /etc/hosts file, the upgrade implodes. Stupid!

I am going to remove ::1 in my builds and see what breaks. I have seen evidence that a minority of developers assume `IPv6 interfaces` exist and don’t bother to support `IPv4`. For $%@#!& sakes why?

{{% /alert %}} <br>

{{% alert "yellow" %}}

<i class="fas fa-bell"></i>&nbsp;**Notice**: I'm running out of reasons to make new alert colors. This is just making CSS larger. `I need to stop`.

{{% /alert %}} <br>

{{% alert "red" %}}

<i class="fas fa-exclamation-triangle"></i>&nbsp;**Warning**: CentOS 7.4.1708 at release the cloud-init 0.7.9 is broken. I have seen number of different bug reports and solutions but no working package has been release as of November 5, 2017. If you have an existing installation using cloud-init 0.7.5 from CentOS 7.3 use the `yum-plugin-versionlock` to lock the version. Version `0.7.5` appears to be working without issue on `7.4`.

{{% /alert %}} <br>

{{% alert "blue" %}}

<i class="fa fa-info-circle"></i>&nbsp;**Note:** Additional Values

1. `NM_MANAGED=no` disables Network Manager for an interface, if using Network Manager
2. `IPV6INIT=no` disables IPv6 for an interface
3. `DEFROUTE=no` or `DEFROUTE=yes` excludes or sets an interface as the default route, respectively, if using Network Manager
4. `PEERDNS=yes` adds the interface’s DNS settings to the `/etc/resolv.conf`
5. PREFIX is an alternative to NETMASK

{{% /alert %}} <br>

{{% alert "purple" %}}

<i class="fas fa-exclamation-circle"></i>&nbsp;**Important**: Internet Explorer is not supported. `If Internet Explorer is your default browser`, copy the URL from Internet Explorer and paste it into Microsoft Edge.

{{% /alert %}}

# OpenNMS
OpenNMS is a free and open-source enterprise grade network monitoring and network management platform. It is developed and supported by a community of users and developers and by the OpenNMS Group, offering commercial services, training and support.

Free
: as in beer
: as in speech

The goal is for OpenNMS to be a truly distributed, scalable management application platform for all aspects of the FCAPS network management model while remaining 100% free and open source. Currently the focus is on Fault and Performance Management.

All code associated with the project is available under the Affero General Public License.

The OpenNMS Project is maintained by The Order of the Green Polo.

{{< disclaimer >}}

# History

The OpenNMS Project was started in July, 1999 by Steve Giles, Brian Weaver and Luke Rindfuss and their company PlatformWorks.[^1] It was registered as project 4141 on Sourceforge in March 2000.[^2] On September 28, 2000, PlatformWorks was acquired by Atipa, a Kansas City-based competitor to VA Linux Systems.[^3] In July 2001, Atipa changed its name to Oculan.[^4]

In September 2002, Oculan decided to stop supporting the OpenNMS project. Tarus Balog, then an Oculan employee, left the company to continue to focus on the project.[^5]

[^1]: http://investing.businessweek.com/research/stocks/private/snapshot.asp?privcapId=433715
[^2]: http://www.adventuresinoss.com/?p=1515
[^3]: http://news.cnet.com/Bluebird-sings-to-Linux-seller-Atipa/2100-1001_3-246408.html
[^4]: http://www.highbeam.com/doc/1G1-76636048.html
[^5]: http://www.adventuresinoss.com/?p=997

In September 2004, The OpenNMS Group was started by Balog, Matt Brozowski and David Hustace to provide a commercial services and support business around the project. Shortly after that, The Order of the Green Polo (OGP) was founded to manage the OpenNMS Project itself. While many members of the OGP are also employees of The OpenNMS Group, it remains a separate organization.

An article of mine! [^myarticle]

Also an [inline link](/posts/2018/96/an-example-post/)!

[^myarticle]: <a href="{{< base-url >}}posts/2018/96/an-example-post/" target="_blank">{{< base-url >}}posts/2018/96/an-example-post/</a>

![Testing a picture](/img/posts/test.png)

# Code snippet
## from https://github.com/OpenNMS/opennms/blob/develop/compile.pl

```perl
#!/usr/bin/env perl

use Cwd qw(abs_path);
use File::Basename qw(dirname);
use File::Spec;

# include script functions
use vars qw(
    $PREFIX
);
$PREFIX = abs_path(dirname($0));
require(File::Spec->catfile($PREFIX, 'bin', 'functions.pl'));


if (not grep { $_ =~ /^[^-]/ } @ARGS) {
    debug("no maven targets specified, adding '" . join("', '", @DEFAULT_GOALS) . "' to the command-line");
    push(@ARGS, @DEFAULT_GOALS);
}

my @command = ($MVN, @ARGS);
info("running:", @command);
handle_errors_and_exit(system(@command));
```
<sup>This is supertext!</sup>

<sub>And this is subtext!!!!!!!!!!!!!!!</sub>

<sub class="inline">And this is subtext with custom margins in CSS.</sub>

<sup class="code">This is supertext above a code block!</sup>

{{< highlight sh "linenos=table,hl_lines=1 12-14 16,linenostart=1" >}}

<key name="PaletteXYZ" modified="2019-01-10 20:13:33" build="180528">
    <value name="Name" type="string" data="Relaxed"/>
    <value name="TextColorIdx" type="hex" data="10"/>
    <value name="BackColorIdx" type="hex" data="10"/>
    <value name="PopTextColorIdx" type="hex" data="10"/>
    <value name="PopBackColorIdx" type="hex" data="10"/>
    <value name="ColorTable00" type="dword" data="00151515"/>
    <value name="ColorTable01" type="dword" data="00ffdfc9"/>
    <value name="ColorTable02" type="dword" data="00639d90"/>
    <value name="ColorTable03" type="dword" data="0099876a"/>
    <value name="ColorTable04" type="dword" data="005356bc"/>
    <value name="ColorTable05" type="dword" data="009866b0"/>
    <value name="ColorTable06" type="dword" data="007adaeb"/>
    <value name="ColorTable07" type="dword" data="00d9d9d9"/>
    <value name="ColorTable08" type="dword" data="00d9d9d9"/>
    <value name="ColorTable09" type="dword" data="00dac19a"/>
    <value name="ColorTable10" type="dword" data="008bdac9"/>
    <value name="ColorTable11" type="dword" data="00ffeee3"/>
    <value name="ColorTable12" type="dword" data="007b7edb"/>
    <value name="ColorTable13" type="dword" data="00d3a5e9"/>
    <value name="ColorTable14" type="dword" data="00a5e7f3"/>
    <value name="ColorTable15" type="dword" data="00f7f7f7"/>
</key>

{{< / highlight >}}<span class="brsmall"></span>
<sub class="code">And this is subtext after a code block.</sub>

# Features

OpenNMS describes itself as a "network management application platform". While useful when first installed, the software was designed to be highly customizable to work in a wide variety of network environments.

There are four main functional areas of OpenNMS.

## Event `Management` and Notifications
OpenNMS is based around a "publish and subscribe" message bus. Processes within the software can publish events, and other processes can subscribe to them. In addition, OpenNMS can receive events in the form of SNMP Traps, Syslog messages, TL/1 events or custom messages sent as XML to port 5817.

Events can be configured to generate alarms. While events represent a history of information from the network, alarms can be used to create correlation workflow (resolving "down" alarms when matching "up" alarms are created) and performing "event reduction" by representing multiple, identical events as a single alarm with a counter. Alarms can also generate events of their own, such as when an alarm is escalated in severity. Alarms clear from the system over time, unlike events that persist as long as desired.

### Alarm subsystem

The Alarm subsystem can also integrate with a variety of trouble ticketing systems, such as Request Tracker, OTRS, Jira, Quickbase and Concursive.

The software also contains an Event Translator where incoming events can be augmented with additional data (such as the impact to customers) and turned into new events.

#### Notifications

Events can generate notifications via e-mail, SMS, XMPP and custom notification methods.

OpenNMS has been shown to be able to process 125,000 syslog messages per minute, continuously.

{{% update "21 Feb 2019" br %}}

This is a single line update.

{{% /update %}}

## Discovery and Provisioning
OpenNMS contains an advanced provisioning system for adding devices to the management system. This process can occur automatically by submitting a list or range of IP addresses to the system (both IPv4 and IPv6). Devices can also be expressly added to the system.

The underlying technology for this configuration is XML, so users can either use the web-based user interface or they can automate the process by scripting the creation of the XML configuration files.

The provisioning system contains adapters to integrate with other processes within the application and to external software, such as a Dynamic DNS server and RANCID.

The provisioning process is asynchronous for scalability, and has been shown to provision networks of more than 50,000 discrete devices and to networks of single devices with over 200,000 virtual interfaces, each (Juniper E320).

{{% update "21 Feb 2019" %}}

This is a multi-line update.

Praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.

{{% /update %}}

{{% update "21 Feb 2019" br %}}

This is a single line update.

{{% /update %}}

## Service Monitoring
The service assurance features of OpenNMS allow for the availability of network-based services to be determined. The types of monitors span from the very simple (ICMP pings, TCP port checks) to the complex (Page Sequence Monitoring, Mail Transport Monitor). Outage information is stored in the database and can be used to generate availability reports.

In addition to being able to monitor network services from the point of view of the OpenNMS server, remote pollers can be deployed to measure availability from distant locations.

Papa John's Pizza uses the OpenNMS remote poller software in each of its nearly 3000 retail stores to measure the availability of centralized network resources.

> Better pizza. Better ingredients.

>> Papa John's

>>> hello there

>>>> how deep does the rabbit hole go

>>>>> even deeper

>>>>>> jeez

>>>>>>> who would need this many nested quote levels

>>>>>>>> is the text getting larger?

>>>>>>>>> I think so

>>>>>>>>>> Defiantly

>>>>>>>>>>> Definitely

>>>>>>>>>>>> Differently

Data Collection
Performance data collection exists in OpenNMS for a number of network protocols including SNMP, HTTP, JMX, WMI, XMP, XML, NSClient, and JDBC. Data can be collected, stored, graphed and checked against thresholds.

The process is highly scalable, and one instance of OpenNMS is collecting 1.2 million data points via SNMP every five minutes.

## Additional Features
OpenNMS is accessed via a web-based user interface built on Jetty. An integration with JasperReports creates high level reports from the database and collected performance data.

<center>

<hr>

|Cool software | Not cool software |
|:---------:|:----------:|
| OpenNMS | PRTG |
| Nagios | PRTG |
| Zabbix | PRTG |

</center>

<hr>

<div style="font-size: 3em;"><center>JS directory</center></div><br>

{{< directoryindex-kb path="/static/js" pathURL="/js" >}}

<hr>

<div style="font-size: 3em;"><center>Post covers directory</center></div><br>

{{< directoryindex-kb path="/static/img/posts/covers" pathURL="/img/posts/covers"  >}}

<hr>

Don't forget about WoW!

<a href="http://www.wowhead.com/item=87768" data-wowhead="item=87768">My favorite mount</a>

<hr>

<div style="font-size: 3em;">Figure:</div><br>

{{< figure src="/img/posts/test.png" title="Test figure" caption="This is a caption" alt="This is the alt text" link="http://oct8l.com" target="_blank">}}

<hr>

GitHub gist:

{{< gist spf13 7896402 >}}

<hr>

Asciicast:

{{< asciicast 165382 >}}

<hr>

Speakerdeck:

{{< speakerdeck 3785e0e40d764c23a7d95c10ea5cbf92 "1.33333333333333" >}}

<hr>

Twitter (simple):

{{< twitter_simple 877500564405444608 >}}

<hr>

Box.com:

{{< box-preview id="37anazvypwmv0y47y4rufjear8barrud" height="200px" >}}

<hr>

Youtube:

{{< youtube w7Ft2ymGmfc >}}

<hr>

Vimeo:

{{< vimeo 146022717 >}}

<hr>
