---
title: "Enabling Talos OS SecureBoot on HP EliteDesk 800 G5"
date: 2025-02-15T19:28:19-06:00

draft: false

toc: true

fontawesome: false

ogVideo:
ogAudio:
ogImage:

tags: ["talos-os","homelab","kubernetes"]
categories: ["homelab"]
keywords: ["talos","talos os","secureboot","elitedesk","hp elitedesk 800 g5"]
seoDescription:

summary:

slug:

url:

### optional cover photo params ###
# covertitle: false
# cover: "/img/posts/covers/2025-26-secureboot-talos.png"
# coveropacity: 30%
# covertext:
# coverheight: 20vh
---

I've recently purchased a few HP EliteDesk 800 G5 desktops for my new homelab v2. This is to supplement my existing UnRAID setup with things running in containers, and some cronjobs on my main desktop that I sometimes have powered off. I'll hopefully write some more about the whole new setup later, but for now, I just want to focus on getting my compute set up (securely) with Talos OS on my new nodes.

## The problem
By default, the HP EliteDesk 800 G5s wouldn't boot from the SecureBoot version of Talos OS from the [image factory](https://factory.talos.dev/) that I've burned to a flash drive. It would only give an error message of "Selected boot image did not authenticate":

![Error message](/img/posts/2025-26-secureboot-talos/initial-error.png)

### Why?
HP's UEFI on the EliteDesk 800 G5s doesn't support the SecureBoot certificate used by the boot image from Sidero Labs right out of the box. The certificate used by the Image Factory Talos OS ISO isn't part of the default trust chain, so SecureBoot fails (by design). This is actually a feature and not a bug, as SecureBoot is designed to prevent malicious software from changing anything in the boot process.

To quote Microsoft (who plays a significant role in the SecureBoot ecosystem):

> To prevent malware from abusing these options, the user must manually configure the UEFI firmware to trust a non-certified bootloader or to turn off Secure Boot. [^1]

[^1]: [Securing the Windows 10 Boot Process](https://learn.microsoft.com/en-us/windows/security/operating-system-security/system-security/secure-the-windows-10-boot-process#:~:text=To%20prevent%20malware%20from%20abusing%20these%20options%2C%20the%20user%20must%20manually%20configure%20the%20UEFI%20firmware%20to%20trust%20a%20non%2Dcertified%20bootloader%20or%20to%20turn%20off%20Secure%20Boot.)

## The solution
We need to modify our UEFI to trust the Sidero Labs certificate. Luckily, Sidero Labs has some excellent and comprehensive documentation on this process[^2]. While it doesn't have exact instructions for the HP EliteDesk UEFI, it does have a general guide for adding a new certificate to the UEFI trust chain. And with enough poking around, we can make this work!

[^2]: [Sidero Labs SecureBoot (v1.9)](https://www.talos.dev/v1.9/talos-guides/install/bare-metal-platforms/secureboot/)

We can restart our machine, and start spamming the escape key to get into the BIOS setup.

![BIOS setup](/img/posts/2025-26-secureboot-talos/bios-setup.png)

### Disable Secure Boot Keys Protection
Once we're in the BIOS setup screen, we should navigate to the Security tab at the top, and the select the "BIOS Sure Start" option. Once we're in the BIOS Sure Start screen, we can uncheck the "Sure Start Secure Boot Keys Protection" option. Be sure to use the enter key to select the option, as the "normal" way of using the spacebar doesn't work to select options in the HP BIOS setup.

![BIOS Secure Boot](/img/posts/2025-26-secureboot-talos/bios--security--bios-sure-start.png)

We'll need to save this change and restart the machine. Hit escape twice (to leave the submenus) and be sure to select "Yes" to saving the changes.

![Save changes](/img/posts/2025-26-secureboot-talos/bios--save-changes.png)

When the system restarts, the Sure Start Secure Boot Keys Protection configuration will prompt us to enter a PIN to disable the protection. This is a security feature that prevents someone/something from remotely changing the SecureBoot configuration without us knowing, and requires physical access to the machine.

[^3]: [HP Community: Cannot resolve "Selected boot image did not authenticate" issue in BiOS](https://h30434.www3.hp.com/t5/Desktop-Boot-and-Lockup/Cannot-resolve-quot-Selected-boot-image-did-not-authenticate/m-p/8928815/highlight/true#M79486)

![PIN prompt](/img/posts/2025-26-secureboot-talos/pin-prompt.png)

Enter the PIN shown on the screen (with your number row on the keyboard[^3]) and hit enter.

After entering the PIN, you can start spamming the escape key to get back to the menu to select the BIOS Setup option.

### Import Custom Secure Boot Keys option
Now we can head to the "Advanced" tab at the top, and select the "Secure Boot Configuration" option. Here, we'll want to check the box to "Import Custom Secure Boot Keys", and then hit escape twice to save and exit again.

![Secure Boot Configuration](/img/posts/2025-26-secureboot-talos/bios--advanced--secure-boot-configuration.png)

We now will get another prompt to enter a PIN. Enter the PIN shown on the screen again, and hit enter.


After this restart, I get a message about the Secure Boot keys failing to import, but this should be fine because we aren't trying to import keys from a USB drive or from the `\EFI\HP\` path the error message mentions. You can either wait for the timeout, or hit enter to continue booting.

![Secure Boot keys import error](/img/posts/2025-26-secureboot-talos/secure-boot-keys-import-error.png)

After this reboot, we'll once again receive a PIN prompt. Enter the PIN, and hit enter.

We now should automatically boot from the Talos OS ISO. If you don't go to the Talos OS ISO at this step, you can manually select it as the boot device by hitting escape to get to the menu and select the "Boot Menu" option, then choosing your device with the ISO on it.

If you see the initial Talos OS boot menu, we've done it!

### Talos OS Secure Boot Enrollment

![Talos OS boot menu](/img/posts/2025-26-secureboot-talos/initial-talos-os-boot.png)

We'll want to select the "Enroll Secure Boot keys: auto" option, and then hit enter.

We'll get a disclaimer about the risk of soft bricking the machine, but if you're confident in the change that'll be made, you can let the timer hit 0 and let the keys be installed.

![Soft bricking disclaimer](/img/posts/2025-26-secureboot-talos/talos-os-key-install-disclaimer.png)

After the keys are installed, you'll get a message saying that the keys have been enrolled, and that the system will now restart.

Now, we're in the home stretch. The system will boot back up to the Talos OS ISO, and we should see in the top banner that we have SECUREBOOT showing "True"!

![SecureBoot True](/img/posts/2025-26-secureboot-talos/secure-boot-talos-banner.png)

You should now be able to install Talos OS. Refer to the [Talos OS documentation](https://www.talos.dev/v1.9/introduction/getting-started/) for more information on how to do this.

### Re-securing the BIOS options
Now that we have Talos OS installed and running, we'll want to re-secure the BIOS options to re-secure the machine. Restart one more time, and spam the escape key to get to the BIOS setup screen.

Once we're in the BIOS setup screen, we should navigate back to the Security tab at the top, and the select the "BIOS Sure Start" option. Once we're in the BIOS Sure Start screen, we can check the "Sure Start Secure Boot Keys Protection" option. We can then hit escape twice to save and exit again.

![Re-securing BIOS options](/img/posts/2025-26-secureboot-talos/bios--security--bios-sure-start--reenable.png)

Now, we're back to the same state (security-wise at least) as before we started the SecureBoot process. We have a machine running Talos OS that is protected by SecureBoot!

## The result
We now have a machine running Talos OS that is protected by SecureBoot! We also have a machine that will require lots of physical access to change the SecureBoot configuration again, and should give some peace of mind.

## The future
I'll hopefully write more about the rest of my new homelab setup soon, but for now, I'm happy to have a cluster of Talos OS nodes that are protected by SecureBoot!
