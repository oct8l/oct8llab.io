---
title: "Disable IPv6 in Ubuntu 14.04 or 16.04"
date: 2019-01-02T12:46:51-06:00

draft: false

keywords: ["ipv6","disable","ubuntu"]

fontawesome: false

seoDescription: If you'd like to disable IPv6 for whatever reason on Ubuntu 14.04 or 16.04, give this one-liner a go.
summary: If you'd like to disable IPv6 for whatever reason on Ubuntu 14.04 or 16.04, give this one-liner a go.

slug:
url:
---

If you'd like to disable IPv6 for whatever reason on Ubuntu 14.04 or 16.04, give this one-liner a go:

``` sh
sudo /bin/su -c "echo -e '\nnet.ipv6.conf.all.disable_ipv6 = 1\nnet.ipv6.conf.default.disable_ipv6 = 1\nnet.ipv6.conf.lo.disable_ipv6 = 1' >> /etc/sysctl.conf" && sudo sysctl -p && cat /proc/sys/net/ipv6/conf/all/disable_ipv6
```

It will echo three lines to the end of `/etc/sysctl.conf`, reload `sysctl`, and then `cat` the file `/proc/sys/net/ipv6/conf/all/disable_ipv6` to let you know if IPv6 has been disabled (`1`) or not (`0`).