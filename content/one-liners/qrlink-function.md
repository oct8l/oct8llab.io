---
title: "qrlink function"
date: 2019-07-15T14:42:47-05:00

draft: false

keywords:

fontawesome: false

ogImage:

seoDescription: If you're like me and want to access a URL on your phone vs your main computer, you might find this useful.
summary: If you're like me and want to access a URL on your phone because you're signed into different accounts on your phone vs your main computer, you might find this useful.

slug:
url:

---

If you're like me and want to access a URL on your phone because you're signed into different accounts on your phone vs your main computer, you might find this useful.

Install `qrencode` for your distribution, and then paste this into your terminal, then re-open it to get the new `~/.profile` settings to apply.

```sh
printf 'qrlink () {\nprintf "$1" | qrencode -t ansi\n}' >> ~/.profile
```

You can now enter `qrlink` in your terminal to generate a QR code that you can scan with your phone and access something like an Amazon.com link (not at all related to why I did this today 😉)

![QR code example](/img/posts/one-liners-qrlink.png)