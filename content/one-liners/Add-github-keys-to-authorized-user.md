---
title: "Add GitHub SSH keys to authorized_user file"
date: 2019-01-07T14:21:49-06:00

draft: false

fontawesome: true

ogImage:

seoDescription: If you have an SSH key you regularly use elsewhere added to your GitHub account, this one-liner to get those public keys and add them to your machine.
summary: If you have an SSH key you regularly use elsewhere added to your GitHub account, this one-liner to get those public keys and add them to the `~/.ssh/authorized_keys` file of your current machine.

slug:
url:
---

If you have an SSH key you regularly use elsewhere added to your GitHub account, this one-liner to get those public keys and add them to the `~/.ssh/authorized_keys` file of your current machine. It will also create the `~/.ssh/authorized_keys` file if your current machine doesn't have one.

{{% alert "yellow" %}}

<i class="fas fa-bell"></i> **Notice**: Be sure to swap out `$USERNAME` for your GitHub username below!

{{% /alert %}}<br>

``` sh
if ! [[ -f ~/.ssh/authorized_keys ]]; then touch ~/.ssh/authorized_keys; fi && curl https://github.com/$USERNAME.keys | tee -a ~/.ssh/authorized_keys
```
<br>