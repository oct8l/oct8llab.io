---
title: "Update a standalone ESXi server"
date: 2018-11-27T13:37:32-06:00

draft: false

keywords: ["esxi","update","esxcli"]

fontawesome: false

seoDescription: Even though it's not the proper way to do it in production, sometimes it's just nice to be able to update ESXi from SSH.
summary: Even though it's not the proper way to do it in production, sometimes it's just nice to be able to update ESXi from SSH, especially an individual instance at home or in a lab.

slug:
url:
---

Even though it's not the proper way to do it in production, sometimes it's just nice to be able to update ESXi from SSH, especially an individual instance at home or in a lab.

First head over to the [awesome list from Virten.net](https://www.virten.net/vmware/vmware-esxi-image-profiles/) and copy the latest Image Profile name (or reference [the table at VMware's KB](https://kb.vmware.com/s/article/2143832) and do a little googling to find the Image Profile for the latest release) available for [your hardware](https://www.vmware.com/resources/compatibility/search.php?deviceCategory=server&details=1&page=1&display_interval=10&sortColumn=Partner&sortOrder=Asc).

Then, plug in the Image Profile into this one-liner, and hit enter:

``` sh
esxcli software profile update -d https://hostupdate.vmware.com/software/VUM/PRODUCTION/main/vmw-depot-index.xml -p ~~~FULL-PROFILE-NAME-HERE~~~ --dry-run
```

Make sure everything looks kosher, and then remove the `--dry-run` portion at the end, and re-run it.

Done!