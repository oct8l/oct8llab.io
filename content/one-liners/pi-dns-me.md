---
title: "Quick pi-dns.me setup"
date: 2019-07-23T16:12:25-05:00

draft: false

keywords: ["dynamic dns","ddns","raspberry pi"]

fontawesome: false

ogImage:

seoDescription: Use this one-liner to generate a dynamic DNS entry, and add it to a cron job automatically.
summary: Enter this one-liner into a BASH prompt, and it will generate a random string of 16 characters, create the name with pi-dns.me, update your current IP and make a cron entry to update it every 15 minutes.

slug:
url:

---

{{% alert "red" %}}

<i class="fas fa-exclamation-triangle"></i>&nbsp;**Warning**: It appears that pi-dns.me is no longer an active service, and attempts to load some sort of parking page when browsed to.

{{% /alert %}} <br>

Enter this one-liner into a BASH prompt, and it will generate a random string of 24 characters, create the name with pi-dns.me, update your current IP and make a `cron` entry to update it every 15 minutes. It will also output your custom dynamic DNS URL to copy from the terminal.

```sh
DDNSSTRING=`head /dev/urandom | tr -dc a-z0-9 | head -c 24 ; echo ''` && DDNSCODE=`/usr/bin/curl -s "https://api.pi-dns.me/v1/beta/create?name=$DDNSSTRING" | awk '{ print $2 }' | awk -F'"' '{print $2}'` && /usr/bin/curl "https://api.pi-dns.me/v1/beta/update?name=$DDNSSTRING&secret=$DDNSCODE"> /dev/null 2>&1 && (crontab -l 2>/dev/null; echo "*/15 * * * * "/usr/bin/curl "https://api.pi-dns.me/v1/beta/update?name=$DDNSSTRING&secret=$DDNSCODE""") | crontab - && printf "\n>>>>>>>>Use $DDNSSTRING.pi-dns.me for dynamic DNS<<<<<<<<\n\n"
```

I know it's not the prettiest, but I wanted to get something that would work so I can document my backup solution (which uses WireGuard and DDNS) in a fashion that would allow me to copy and paste every configuration change, instead of editing files manually. There's really no reason other than I'd like the technical and documentation practice. 😊