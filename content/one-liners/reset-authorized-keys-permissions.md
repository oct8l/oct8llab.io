---
title: "Reset authorized_keys permissions"
date: 2018-07-19T10:50:17-05:00

draft: false

fontawesome: false

seoDescription: Sometimes, you might find you are having odd issues using public keys on a Linux server. Give this a shot, and it should fix up the account you run it as.
summary: Sometimes, you might find you are having odd issues using public keys on a Linux server. It will fix any permissions issues that you might have caused by editing the files incorrectly.

slug:
url:
---

Sometimes, you might find you are having odd issues using public keys on a Linux server. I usually end up editing files under the wrong account and mess up permissions.

The solution _might_ be to run this one-liner (really an adaptation of [multiple commands](http://www.thelinuxdaily.com/2012/02/proper-authorized_keys-permissions-for-passwordless-ssh-access/), one-liner is a questionable name for this) as the user who you're trying to log in to the server as (probably not `root`):

``` sh
chmod 700 $HOME/.ssh && chmod go-w $HOME $HOME/.ssh && chmod 600 $HOME/.ssh/authorized_keys && chown `whoami` $HOME/.ssh/authorized_keys
```
<br>