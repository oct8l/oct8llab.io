---
title: "Add ssht to Linux"
date: 2019-04-17T13:28:25-05:00

draft: false

keywords:

fontawesome: false

ogImage:

seoDescription: A quick one-liner to add a binary file that will launch tmux automatically upon an SSH connection.
summary: A quick one-liner to add a binary file that will launch tmux automatically upon an SSH connection.

slug:
url:

---

I recently ran across [ssht](https://github.com/brejoc/ssht) (via [Stack Overflow](https://stackoverflow.com/questions/27613209/how-to-automatically-start-tmux-on-ssh-session), of course) when trying to figure out how to launch `tmux` automatically upon an SSH connection.

Enter this into a BASH prompt (assuming `/usr/bin/` is in your PATH), and you'll be good to go to use `ssht` as a drop-in replacement for `ssh`!

```
sudo curl https://raw.githubusercontent.com/brejoc/ssht/master/ssht -o /usr/bin/ssht && sudo chmod +x /usr/bin/ssht
```