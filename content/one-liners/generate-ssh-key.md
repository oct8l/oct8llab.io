---
title: "Generate SSH key"
date: 2018-06-19T10:36:30-05:00

draft: false

keywords: ["generate","ssh","key"]

seoDescription: A quick one-liner to generate a secure SSH key in Bash.

summary: A quick one-liner to generate a secure SSH key in Bash.
url:

fontawesome: false
---

Replace `$COMMENT` with whatever comment you want the key to have:

``` sh
ssh-keygen -t rsa -b 4096 -C "$(whoami)@$(hostname)--$(date -I)--$COMMENT"
```
<br>