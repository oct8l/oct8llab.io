---
title: "Home"
draft: false
keywords: ["oct8l","tech","blog"]
seoDescription: The home page of oct8l, built with Hugo and hosted on GitLab pages
---

Thanks for the visit!

This site will serve as a repository for any knowledge I gather in my professional career or in my private endeavors that I think is worth sharing. At times, it may just be a log of projects that I've worked on at home for fun.

I have moved into a more development-focused role at my current employer, but I still have a passion for small-scale, non-mission critical systems administration (mostly at home these days).

In my previous role, I didn't get to to focus a lot on project work, and instead was fighting fires often. Now, I have been afforded the luxury of working on a more narrow subset of tasks and mastering specific topics, which I am very thankful for. I hope to be able to contribute back to the libre software community once I start really getting into the weeds into my current role.

The content of this site is licensed under {{< license >}} unless otherwise specified, and any code supplied in posts or other pages is under {{< code-license >}} unless otherwise specified. Please feel free to re-share anything you find that I've put up on this site. If you would like to contact me about any concerns or questions, please&nbsp;{{< contact-me text="email me">}}.

Feel free to [search](/search) for specific content or click through the menus above to check out what I've come up with so far.
<br><br><br>
You could also check out my [category cloud](/category-cloud/), [tag cloud](/tag-cloud/), or some of my most recent posts: