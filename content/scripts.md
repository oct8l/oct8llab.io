---
title: "Scripts Directory"
draft: false
toc: false
tags: []
categories: []
keywords: []
seoDescription:
fontawesome: true
summary:
slug:
url:
---
{{% alert "red" %}}

<i class="fas fa-exclamation-triangle"></i>&nbsp;**Warning**: Before downloading anything from this page, confirm that you are visiting the site via the http**_s_** protocol.

{{% /alert %}}<br>

{{< disclaimer >}}

You can check out some information about getting and running scripts and checksums from my site site <a href="/running-scripts/">here.</a>

{{< directoryindex-b path="/static/scripts" pathURL="/scripts" >}}