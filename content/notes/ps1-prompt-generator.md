---
title: "PS1 prompt generator"
date: 2019-02-23T21:01:39-06:00

draft: false

toc: false

fontawesome: false

seoDescription:
summary:

slug:
url:

---

A real cool site for generating a [PS1](https://ss64.com/bash/syntax-prompt.html) (prompt string) in Linux is [ezprompt.net](http://ezprompt.net/). It's an awesome resource for generating your ideal prompt, and then dropping it quickly into your `.bashrc` file and running a `source ~/.bashrc` to get your new prompt.

Try it out!