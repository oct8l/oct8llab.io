---
title: "Dell R210 ii not booting"
date: 2018-07-05T11:53:27-05:00

draft: false

toc: false

fontawesome: false

seoDescription:
summary:

slug: dell-r210-ii-not-booting
url:
---

I thought I'd take the extra NIC out of a server and mount it back up.

Note to self, the Dell R210 ii server won't boot unless the riser card is plugged into it...

I'm sure there's a BIOS setting somewhere but I figured it'd be easier to plug it in and leave it.