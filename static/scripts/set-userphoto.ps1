$admUser = "$env:USERNAME"
$imageFolder = "C:\Users\$admUser\Pictures"
$pictures = Get-ChildItem $imageFolder

foreach ($picture in $pictures){
    
    Try {
        $user = Get-User -Identity $($picture.BaseName) -ErrorAction Stop
        }
    
    Catch {
        Write-Warning "Warning: $_"
    }

    If ($user) {
        $user | Set-UserPhoto -PictureData ([System.IO.File]::ReadAllBytes("$imageFolder\$($picture.Name)")) -Confirm:$false
    }
}