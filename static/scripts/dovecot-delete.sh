#!/bin/bash

# Written by oct8l (www.oct8l.com)
# https://www.apache.org/licenses/LICENSE-2.0

#
#

# This first if statment handles if you want to run the script to check if the
# files exist in the mailbox directory

# The syntax will be: ./unlock check /home/vpopmail/domains/[domain]/2/O/abc,123
#(if you were wanting to check the mailbox of my account)

if [ "$1" == "check" ]; then
	ls -l ${2}/Maildir | grep 'dovecot.index.log*\|dovecot.index.cache*\|dovecot.index.lock*' | awk '{print " "$6" "$7" "$8" "$9" "}' && exit;
fi


# This is the main part of the script to prompt you for the path of the user's
# mailbox that you can find in the Magic Mail admin page under Mailbox Info

echo "Enter the mailbox's path"
read mailboxpath
cd $mailboxpath/Maildir

# This will tell you what will be deleted by the script if you give confirmation
# in the next step

echo 
echo "-------------------------------------------------"
echo "Files up for deletion:"
echo 
ls -l | grep 'dovecot.index.log*\|dovecot.index.cache*\|dovecot.index.lock*' | awk '{print " "$6" "$7" "$8" "$9" "}'
echo 
echo "-------------------------------------------------"
echo 

# This is where you can answer y, yes, Yes, yEs, yeS, YeS, or YES if you'd like
# to go ahead and delete the files, or the script will exit if any other input
# is received
read -r -p "Would you like to delete dovecot.index.log, dovecot.index.cache and dovecot.index.lock? [y/N] " response
if [[ "$response" =~ ^([yY][eE][sS]|[yY])+$ ]]
then
    rm dovecot.index.log* ; rm dovecot.index.cache ; rm dovecot.index.lock ; ls -l | grep 'dovecot.index.log*\|dovecot.index.cache*\|dovecot.index.lock*' | awk '{print " "$6" "$7" "$8" "$9" "}';
else
    echo "Exiting script" && exit;
fi

exit

