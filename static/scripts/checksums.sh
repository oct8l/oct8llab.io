md5sum $1  > $1_checksum.txt && \
    sha1sum $1  >> $1_checksum.txt &&     
    sha256sum $1  >> $1_checksum.txt && \
    sha512sum $1 >> $1_checksum.txt