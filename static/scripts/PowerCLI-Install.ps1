### Variable definitions

$downloadsFolder = [Environment]::GetFolderPath("Desktop")

$psModulesFolder = [Environment]::GetFolderPath("mydocuments")

$powerCliExists = Test-Path -LiteralPath "C:\Program Files (x86)\VMware\Infrastructure\PowerCLI\"

###

### Check for old versions of PowerCLI

If ($powerCliExists -eq $False) {
    Write-Host ""
    Write-Host " No prior versions of PowerCLI detected " -ForegroundColor Black -BackgroundColor Green
    Write-Host ""
}
Else {
    Write-Host ""
    Write-Host " Please remove prior installs of PowerCLI before continuing. Re-run script when this is deleted. " -ForegroundColor Black -BackgroundColor Red
    Write-Host ""
    Exit
}

###

### Grab files for offline install

New-Item -ItemType directory -Path $downloadsFolder\PowerCLI-install >$null

Write-Host ""
Write-Host "Respond with 'y' at the next step when prompted to allow downloading from NuGet. . ."
Write-Host ""

Save-Module -Name VMware.PowerCLI -Path $downloadsFolder\PowerCLI-install

###

### Copy files to modules folder

Copy-Item -Path "$downloadsFolder\PowerCLI-install\*" -Destination "$psModulesFolder\WindowsPowerShell\Modules" -Recurse -Force

###

### Cleanup

Remove-Item -Path "$downloadsFolder\PowerCLI-install" -Recurse -Force

###

###

Write-Host " Installation is complete! " -ForegroundColor Black -BackgroundColor Green
Write-Host " You may need to restart any programs to use the new modules. " -ForegroundColor Black -BackgroundColor Green

###