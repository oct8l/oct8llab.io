#!/bin/sh

# Information from openwrt.org wiki:
# https://wiki.openwrt.org/doc/howto/wget-ssl-certs
#
# Script written by oct8l (www.oct8l.com)
#
# Script licensed as CC BY-NC-SA 3.0
# (https://creativecommons.org/licenses/by-nc-sa/3.0)

# Make the directory for the SSL certs
mkdir -p /etc/ssl/certs
# Set the directory so wget knows where to look
export SSL_CERT_DIR=/etc/ssl/certs
# Reload the shell after the last step
source /etc/profile 
#Update packages and install the newest version of wget along with certificates
#and utilities for SSL
opkg update
opkg install wget
opkg install openssl-util
#opkg install ca-certificates # Uncomment if  running LEDE 17.01.4 or later
#opkg install ca-bundle # Uncomment if  running LEDE 17.01.4 or later
#opkg install libustream-openssl # Uncomment if  running LEDE 17.01.4 or later
echo
echo "--------------------------------"
echo "You can now wget from https URLs"
echo "--------------------------------"
