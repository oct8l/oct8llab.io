This is my blog site. Hopefully it will eventually contain some useful information on it.

It's built with [Hugo](https://github.com/gohugoio/hugo) and I try to keep the [Docker image](https://gitlab.com/oct8l/oct8l.gitlab.io/container_registry) up-to-date with the latest Hugo releases. It's minified with some `npm` packages and then generated with the Docker container, and pushed up to the hosted GitLab Pages every commit.

Let me know if you have found anything here useful!